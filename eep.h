/* Set up Arduino/Atmega 328p EEPROM to act as parameter memory
 * Will allow both more modular code AND more configurable behaviors
 */

#ifndef DSESW_EEP_H
#define DSESW_EEP_H

#include "cmn.h"

#define EEP_SIZE 1024
#define EEP_RECORD_MAX_SIZE 32

enum class EEP_RECORDS : U8 {
    SYSTEM_CONFIGURATION,
    SERVO_SOFTSTOPS,
    SERVO_NOMINAL_POSITIONS,
    BEHAVIOR_TABLE_0,
    BEHAVIOR_TABLE_1,
    BEHAVIOR_TABLE_2,
    BEHAVIOR_TABLE_3,
    BEHAVIOR_TABLE_4,
    BEHAVIOR_TABLE_5,
    BEHAVIOR_TABLE_6,
    BEHAVIOR_TABLE_7,
    BEHAVIOR_TABLE_8,
    BEHAVIOR_TABLE_9,
    BEHAVIOR_TABLE_10,
    BEHAVIOR_TABLE_11,
    BEHAVIOR_TABLE_12,
    BEHAVIOR_TABLE_13,
    BEHAVIOR_TABLE_14,
    BEHAVIOR_TABLE_15,
    BEHAVIOR_TABLE_16,
    BEHAVIOR_TABLE_17,
    BEHAVIOR_TABLE_18,
    BEHAVIOR_TABLE_19,
    BEHAVIOR_TABLE_20,
    BEHAVIOR_TABLE_21,
    BEHAVIOR_TABLE_22,
    BEHAVIOR_TABLE_23,
    BEHAVIOR_TABLE_24,
    BEHAVIOR_TABLE_25,
    BEHAVIOR_TABLE_26,
    BEHAVIOR_TABLE_27,
    BEHAVIOR_TABLE_28
};

// Generic blob of memory to represent a single "record" in the EEPROM.
// Client modules can cast this however the heck they want.
struct EepRecord {
    U8 data[EEP_RECORD_MAX_SIZE];
};

// Retrieve a record from EEPROM. 
// This will retrieve the max record size. It is up to clients to figure out how much of that is relevant.
CmnStatus eep_get_record(EEP_RECORDS record_id, EepRecord& record);

// Write a record to EEPROM.
// This will write the entire max record size.
// It is up to the client to encapsulate the data in an EepRecord struct.
CmnStatus eep_write_record(EEP_RECORDS record_id, const EepRecord& record);

// Read an address from EEP.
// Dump Log Message about it.
CmnStatus eep_peek(U16 addr);

// Write directly to an address in EEP.
// **DANGEROUS** - this bypasses the EEP_RECORDS system.
CmnStatus eep_poke(U16 addr, U8 val);

// Dump a whole record via log messages:
CmnStatus eep_dump_record(EEP_RECORDS record_id);

// Wipe an entire EEP record back to 255 (the default value).
// **DANGEROUS** - this will lose your config!
CmnStatus eep_wipe_record(EEP_RECORDS record_id);

#endif
