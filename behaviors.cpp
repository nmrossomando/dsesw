#include "behaviors.h"

#include <Arduino.h>

#include "tlm.h"

BehaviorTable creation_table;
U8 creation_table_id;
U8 current_action_index;
bool table_in_progress;

BehaviorTable execution_table;
U8 execution_table_id;
U8 execution_action_index;
bool behavior_executing;
bool behavior_paused;
U32 behavior_resume_time;

void behaviors_init() {
    current_action_index = 0;
    table_in_progress = false;

    behavior_executing = false;
    behavior_paused = false;
}

void behaviors_main() {
    if(behavior_executing) {
        behaviors_continue_behavior();
    }
}

CmnStatus behaviors_execute_behavior(U8 behavior_id) {
    // Clients should be checking this first, so make this a W_HI if it happens.
    if(behavior_executing) {
        TLM_LOG_MESSAGE(LogLevels::WARNING_HI, LogMessages::BEHAVIORS_LOG_BEHAVIOR_ALREADY_RUNNING, "Aborting attempt to run behavior %u because behavior %u is already running!", behavior_id, execution_table_id);
        return CmnStatus::FAILURE;
    }

    // Clear execution table:
    for(auto& i : execution_table.actions) {
        i.id = ActionTypes::NONE;
        i.arg = 0;
    }

    
    // Load behavior table from EEPROM:
    EepRecord record;
    EEP_RECORDS rec_id = static_cast<EEP_RECORDS>(behavior_id + 3);
    CmnStatus stat = eep_get_record(rec_id, record);

    if(stat == CmnStatus::FAILURE) {
        TLM_LOG_MESSAGE(LogLevels::WARNING_LO, LogMessages::BEHAVIORS_LOG_EEP_LOAD_FAILED, "Failed to load behavior table %u from EEPROM.", behavior_id);
        return CmnStatus::FAILURE;
    }

    memcpy(&execution_table.actions, &record.data, EEP_RECORD_MAX_SIZE);
    
    if(execution_table.actions[0].id == ActionTypes::NONE) {
        TLM_LOG_MESSAGE(LogLevels::WARNING_LO, LogMessages::BEHAVIORS_LOG_TABLE_EMPTY, "Cannot run behavior %u because it has no defined actions!", behavior_id);
        return CmnStatus::FAILURE;
    }

    behavior_executing = true;
    execution_table_id = behavior_id;
    execution_action_index = 0;
    behavior_paused = false;
    behavior_resume_time = 0;

    TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::BEHAVIORS_LOG_BEGIN_BEHAVIOR, "Beginning execution of behavior id %u", execution_table_id);
    stat = behaviors_run_next_step();
    if(stat == CmnStatus::FAILURE) {
        behaviors_stop_behavior(stat);
        return stat;
    }

    execution_action_index++;
    return stat;
}

CmnStatus behaviors_continue_behavior() {
    // Check if behavior is paused and, if so, if we should resume:
    if(behavior_paused) {
        if(millis() >= behavior_resume_time) {
            behavior_paused = false;
        }
        else {
            return CmnStatus::SUCCESS;
        }
    }
    
    // Check if behavior is out of actions, i.e. done:
    if(execution_table.actions[execution_action_index].id == ActionTypes::NONE) {
        behaviors_stop_behavior(CmnStatus::SUCCESS);
        return CmnStatus::SUCCESS;
    }

    // Run next step otherwise:
    CmnStatus stat = behaviors_run_next_step();
    if(stat == CmnStatus::FAILURE) {
        behaviors_stop_behavior(stat);
        return stat;
    }

    execution_action_index++;
    return stat;
}

CmnStatus behaviors_run_next_step() {
    TLM_LOG_MESSAGE(LogLevels::DIAGNOSTIC, LogMessages::BEHAVIORS_LOG_RUN_ACTION, "Running action in row %u of behavior table %u.", execution_action_index, execution_table_id);
    CmnStatus stat = actions_execute_action(execution_table.actions[execution_action_index]);
    return stat;
}

void behaviors_pause_behavior(U32 resume_time) {
    TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::BEHAVIORS_LOG_PAUSING_EXECUTION, "Pausing behavior execution until %u millisec since boot.", resume_time);
    behavior_resume_time = resume_time;
    behavior_paused = true;
}

CmnStatus behaviors_stop_behavior(CmnStatus status) {
    if(behavior_executing == true) {
        if(status == CmnStatus::FAILURE) {
            TLM_LOG_MESSAGE(LogLevels::WARNING_LO, LogMessages::BEHAVIORS_LOG_ABORTING_BEHAVIOR, "Aborting behavior %u at action row %u.", execution_table_id, execution_action_index);
        }
        else {
            TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::BEHAVIORS_LOG_END_BEHAVIOR, "Behavior id %u completed with SUCCESS!", execution_table_id);
        }
    }
    else {
        return CmnStatus::FAILURE;
    }

    execution_table_id = 0;
    execution_action_index = 0;
    behavior_executing = false;
    behavior_paused = false;
    behavior_resume_time = 0;
    return CmnStatus::SUCCESS;
}

bool behaviors_is_behavior_executing() {
    return behavior_executing;
}

CmnStatus behaviors_create_new_behavior(U8 behavior_id) {
    if((behavior_id + 3) > static_cast<U8>(EEP_RECORDS::BEHAVIOR_TABLE_28)) {
        TLM_LOG_MESSAGE(LogLevels::WARNING_LO, LogMessages::BEHAVIORS_LOG_CREATE_ID_INVALID, "Could not create behavior, id %u is larger than max allowed 28!", behavior_id);
        return CmnStatus::FAILURE;
    }
    
    table_in_progress = true;
    creation_table_id = behavior_id;
    current_action_index = 0;

    for(auto& i : creation_table.actions) {
        i.id = ActionTypes::NONE;
        i.arg = 0;
    }

    TLM_LOG_MESSAGE(LogLevels::DIAGNOSTIC, LogMessages::BEHAVIORS_LOG_BEHAVIOR_CREATED, "Began creation of behavior id %u.", behavior_id);

    return CmnStatus::SUCCESS;
}

CmnStatus behaviors_new_behavior_add_action(ActionTypes action, U8 arg=0) {
    if(current_action_index >= 16) {
        TLM_LOG_MESSAGE_NO_ARGS(LogLevels::WARNING_LO, LogMessages::BEHAVIORS_LOG_BEHAVIOR_FULL, "Could not add action to behavior, already full!");
        return CmnStatus::FAILURE;
    }
    else if(table_in_progress == false) {
        TLM_LOG_MESSAGE_NO_ARGS(LogLevels::WARNING_LO, LogMessages::BEHAVIORS_LOG_NO_BEHAVIOR_TO_ADD_TO, "Could not add action because no table is even being created right now!");
        return CmnStatus::FAILURE;
    }

    creation_table.actions[current_action_index].id = action;
    creation_table.actions[current_action_index].arg = arg;
    current_action_index++;

    TLM_LOG_MESSAGE(LogLevels::DIAGNOSTIC, LogMessages::BEHAVIORS_LOG_ACTION_ADDED, "Added action %s with argument %u to behavior %u.", action, arg, creation_table_id);

    return CmnStatus::SUCCESS;
}

CmnStatus behaviors_commit_new_behavior() {
    if(table_in_progress == false) {
        TLM_LOG_MESSAGE_NO_ARGS(LogLevels::WARNING_LO, LogMessages::BEHAVIORS_LOG_NO_BEHAVIOR_TO_COMMIT, "Could not save behavior because no table is even being created right now!");
        return CmnStatus::FAILURE;
    }
    
    // Ah, serialization shenanigans...
    EepRecord record;
    memcpy(&record.data, &creation_table.actions, EEP_RECORD_MAX_SIZE);

    EEP_RECORDS id = static_cast<EEP_RECORDS>(creation_table_id + 3); // Behavior tables start at record index 3.
    CmnStatus stat = eep_write_record(id, record);

    if(stat == CmnStatus::SUCCESS) {
        TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::BEHAVIORS_LOG_SAVED_BEHAVIOR, "Behavior id %u committed to EEPROM!", creation_table_id);
    }
    else {
        TLM_LOG_MESSAGE(LogLevels::WARNING_LO, LogMessages::BEHAVIORS_LOG_EEP_SAVE_FAILED, "Failed to save behavior id %u to EEPROM!", creation_table_id);
    }

    return stat;
}

CmnStatus behaviors_create_cancel() {
    if(table_in_progress == false) {
        TLM_LOG_MESSAGE_NO_ARGS(LogLevels::WARNING_LO, LogMessages::BEHAVIORS_LOG_NO_BEHAVIOR_TO_CANCEL, "No behavior is being created, nothing to cancel.");
        return CmnStatus::FAILURE;
    }

    table_in_progress = false;
    creation_table_id = 0;
    current_action_index = 0;
}
