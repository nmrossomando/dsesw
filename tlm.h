#ifndef TLM_H
#define TLM_H

#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdint.h>
#include "cobs.h"

#include "cmn.h"
#include "UART.h"
#include "servoctl.h"

enum class LogMessages : U8 {
    UART_LOG_RX_OVERRUN,
    UART_LOG_TX_BUFF_NULL,
    UART_LOG_COBS_DECODE_ERROR,
    UART_LOG_RX_PACKET_TOO_LONG,
    UART_LOG_RX_PROCESSED_PACKETS,
    SYS_LOG_INIT,
    SYS_LOG_LOADED_CONFIG,
    BUSCMD_LOG_EXECUTING_COMMAND,
    BUSCMD_LOG_CMD_FAILURE,
    BUSCMD_LOG_CMD_SUCCESS,
    BUSCMD_LOG_BUFFER_FULL,
    BEHAVIORS_LOG_BEHAVIOR_FULL,
    BEHAVIORS_LOG_CREATE_ID_INVALID,
    BEHAVIORS_LOG_NO_BEHAVIOR_TO_ADD_TO,
    BEHAVIORS_LOG_NO_BEHAVIOR_TO_COMMIT,
    BEHAVIORS_LOG_SAVED_BEHAVIOR,
    BEHAVIORS_LOG_EEP_SAVE_FAILED,
    BEHAVIORS_LOG_BEHAVIOR_CREATED,
    BEHAVIORS_LOG_ACTION_ADDED,
    BEHAVIORS_LOG_NO_BEHAVIOR_TO_CANCEL,
    BEHAVIORS_LOG_BEHAVIOR_ALREADY_RUNNING,
    BEHAVIORS_LOG_EEP_LOAD_FAILED,
    BEHAVIORS_LOG_TABLE_EMPTY,
    BEHAVIORS_LOG_BEGIN_BEHAVIOR,
    BEHAVIORS_LOG_RUN_ACTION,
    BEHAVIORS_LOG_ABORTING_BEHAVIOR,
    BEHAVIORS_LOG_END_BEHAVIOR,
    BEHAVIORS_LOG_PAUSING_EXECUTION,
    ACTIONS_LOG_RUNNING_ACTION,
    ACTIONS_LOG_UNDEFINED_ACTION,
    ACTIONS_LOG_NOP,
    SERVO_LOG_SOFTSTOP_EEP_LOAD_FAILED,
    SERVO_LOG_SOFTSTOP_EEP_BLANK,
    SERVO_LOG_POSITION_EEP_LOAD_FAILED,
    SERVO_LOG_POSITION_EEP_BLANK,
    SERVO_LOG_REQUEST_VIOLATES_SOFTSTOP,
    SERVO_LOG_MOVING_TO_POSITION,
    SERVO_LOG_MOVING_TO_OPEN_POS,
    SERVO_LOG_MOVING_TO_CLOSED_POS,
    SERVO_LOG_SHOW_CONFIG,
    SERVO_LOG_ARG_OUT_OF_RANGE,
    SERVO_LOG_SET_SOFTSTOPS,
    SERVO_LOG_SET_OPEN_POS,
    SERVO_LOG_SET_CLOSED_POS,
    SERVO_LOG_SAVE_SOFTSTOPS_FAILED,
    SERVO_LOG_SAVE_POSITIONS_FAILED,
    SERVO_LOG_SAVED_CONFIG_TO_EEP,
    SERVO_LOG_INVALID_SERVO_ID,
    SERVO_LOG_STOP_SERVO,
    SERVO_LOG_STOP_ALL_SERVOS,
    SERVO_LOG_ABORT,
    CMD_LOG_EXECUTED_NO_OP,
    CMD_LOG_RESTRICTIONS_LIFTED,
    CMD_LOG_DISPATCH_RESTRICTED,
    CMD_LOG_BAD_OPCODE,
    CMD_LOG_FAILED_VALIDATION,
    CMD_LOG_DISPATCHING_COMMAND,
    CMD_LOG_COMPLETION_FAILURE,
    CMD_LOG_COMPLETION_SUCCESS,
    EEP_LOG_PEEK_ADDRESS,
    EEP_LOG_POKE_ADDRESS,
    EEP_LOG_RECORD_DUMP,
    EEP_LOG_RECORD_WIPED,
    EEP_LOG_RECORD_WIPE_FAILED,
    CMD_LOG_ECHO,
    DEBUG_LOG_PRINT
};

enum class LogLevels : U16 {
    FATAL,
    WARNING_HI,
    WARNING_LO,
    ACTIVITY,
    COMMAND,
    DIAGNOSTIC
};

extern volatile U8 old_sreg;
extern U8 xmit_buffer[256];
extern U8 xmit_buffer_write_pos;
extern U8 msg_in_progress[20];
extern U8 msg_in_progress_write_pos;

void tlm_init();
void tlm_xmit();
U16 tlm_swap_16(U16 val);

#define TLM_LOG_MESSAGE_NO_ARGS(msg_level, msg_id, fmt) tlm_log_message(msg_level, msg_id)
#define TLM_LOG_MESSAGE(msg_level, msg_id, fmt, ...) tlm_log_message(msg_level, msg_id, true, __VA_ARGS__)

void tlm_log_message(LogLevels msg_level, LogMessages msg_id);
template<typename T> void tlm_log_message(LogLevels msg_level, LogMessages msg_id, bool first_iter, T arg) {
    // If this is the first processing iteration for this log message, do the initial setup:
    if(first_iter == true) {
        first_iter = false; // Only do this once

        old_sreg = SREG; // This is the latest we can safely disable interrupts.
        cli();
        msg_in_progress_write_pos = 3; // Stuff done in here eats positions 0 - 2, always.

        U8* apid = reinterpret_cast<U8*>(&msg_level);
        msg_in_progress[0] = 0;
        msg_in_progress[1] = *apid; // Write the message log level to the buffer. Will always be size 2 (U16).
        msg_in_progress[2] = static_cast<U8>(msg_id); // Write the message id to the buffer. Will always be size 1 (U8).
    }

    // Since there is no argument pack, there is only one argument in this instance. Write it to the buffer.
    // This means it was either the last or only argument.
    U8* start_of_arg = reinterpret_cast<U8*>(&arg);
    U16 arg_size = static_cast<U16>(sizeof(arg));
    U8* end_of_arg = start_of_arg + arg_size - 1;
    
    // Since we need to byteswap for endianness, but the size could be anything, just write the bytes in reverse order...
    // TODO: check if array and don't do this. std::is_array - like.
    for(U16 i=0; i<arg_size; i++) {
        msg_in_progress[msg_in_progress_write_pos] = *end_of_arg;
        end_of_arg--;
        msg_in_progress_write_pos++;
    }

    // Since this is the last iteration by default (as the last argument is processed by this function), do the "cleanup":
    cobs_encode_result result = cobs_encode(&xmit_buffer[xmit_buffer_write_pos], 256-xmit_buffer_write_pos, msg_in_progress, msg_in_progress_write_pos); // cobs encode.
    if(result.status == COBS_ENCODE_OK) {
        xmit_buffer_write_pos += static_cast<U8>(result.out_len);
        xmit_buffer[xmit_buffer_write_pos] = 0;
        xmit_buffer_write_pos++;
    }
    // FATAL deadend:
    if(msg_level == LogLevels::FATAL) {
        // Attempt to transmit the FATAL
        uart_xmit(&xmit_buffer[xmit_buffer_write_pos - static_cast<U8>(result.out_len) - 1], static_cast<U8>(result.out_len) + 1);
        // Abort servo motion:
        servoctl_abort();
        // Deadend:
        for(;;) {
            _delay_ms(1000);
		}
    }
    SREG = old_sreg; // Finally, Re-enable interrupts.
}

template<typename T, typename... Args> void tlm_log_message(LogLevels msg_level, LogMessages msg_id, bool first_iter, T arg, Args const&... args) {
    // If this is the first preocessing iteration for this log message, do the initial setup:
    if(first_iter == true) {
        first_iter = false; // Only do this once

        old_sreg = SREG; // This is the latest we can safely disable interrupts.
        msg_in_progress_write_pos = 3; // Stuff done in here eats positions 0 - 2, always.

        U8* apid = reinterpret_cast<U8*>(&msg_level);
        msg_in_progress[0] = 0;
        msg_in_progress[1] = *apid; // Write the message log level to the buffer. Will always be size 2 (U16).
        msg_in_progress[2] = static_cast<U8>(msg_id); // Write the message id to the buffer. Will always be size 1 (U8).
    }

    // Write the argument to the buffer, serialze to bytes with some tricky casting...
    U8* start_of_arg = reinterpret_cast<U8*>(&arg);
    U16 arg_size = static_cast<U16>(sizeof(arg));
    U8* end_of_arg = start_of_arg + arg_size - 1;
    
    // Since we need to byteswap for endianness, but the size could be anything, just write the bytes in reverse order...
    // TODO: check if array and don't do this. std::is_array - like.
    for(U16 i=0; i<arg_size; i++) {
        msg_in_progress[msg_in_progress_write_pos] = *end_of_arg;
        end_of_arg--;
        msg_in_progress_write_pos++;
    }

    // Since there's still an argument pack here, we aren't done, "recurse" (it isn't actually recursive once compiled)
    tlm_log_message(msg_level, msg_id, false, args...);
}

#endif
