#include "tlm.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdint.h>
#include "cobs.h"

#include "UART.h"

volatile U8 old_sreg;
U8 xmit_buffer[256];
U8 xmit_buffer_write_pos;
U8 msg_in_progress[20];
U8 msg_in_progress_write_pos;

void tlm_init() {
    for(auto& i : xmit_buffer) {
        i = 0;
    }
    xmit_buffer_write_pos = 0;

    for(auto& i : msg_in_progress) {
        i = 0;
    }
    msg_in_progress_write_pos = 0;
}

void tlm_xmit() {
    uart_xmit(xmit_buffer, xmit_buffer_write_pos);
    xmit_buffer_write_pos = 0;
}

U16 tlm_swap_16(U16 val) {
    return (val >> 8) | (val << 8);
}

void tlm_log_message(LogLevels msg_level, LogMessages msg_id) {
    U16 msg_size = 3; // One byte for level, two bytes for id.

    old_sreg = SREG;
    cli();

    U8* apid = reinterpret_cast<U8*>(&msg_level);
    msg_in_progress[0] = 0;
    msg_in_progress[1] = *apid; // Write the message log level to the buffer. Will always be size 2 (U16).
    msg_in_progress[2] = static_cast<U8>(msg_id);

    cobs_encode_result result = cobs_encode(&xmit_buffer[xmit_buffer_write_pos], 256-xmit_buffer_write_pos, msg_in_progress, msg_size); // cobs encode.
    if(result.status == COBS_ENCODE_OK) {
        xmit_buffer_write_pos += static_cast<U8>(result.out_len);
        xmit_buffer[xmit_buffer_write_pos] = 0;
        xmit_buffer_write_pos++;
    }
    // FATAL deadend:
    if(msg_level == LogLevels::FATAL) {
        // Attempt to transmit the FATAL
        uart_xmit(&xmit_buffer[xmit_buffer_write_pos - static_cast<U8>(result.out_len) - 1], static_cast<U8>(result.out_len) + 1);
        // Abort servo motion:
        servoctl_abort();
        // Deadend:
        for(;;) {
            _delay_ms(1000);
		}
    }
    SREG = old_sreg; // Finally, Re-enable interrupts.
}
