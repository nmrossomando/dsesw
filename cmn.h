#ifndef DSESW_CMN_H
#define DSESW_CMN_H

#include <stdint.h>

// Type Shorthands
typedef uint8_t U8;
typedef uint16_t U16;
typedef uint32_t U32;
typedef uint64_t U64;
typedef float F32;
typedef double F64;

// Status enum
enum class CmnStatus : U8 {SUCCESS, FAILURE};

#endif
