/* DSESW - Dome Servo Extender Software
 * AUTO-GENERATED CODE. DO NOT EDIT.
 * 
 * Built By FluidSW Code Generation Tools
 */

/*
 * Command validation functions.
 * Sanity check incoming command arguments to guard
 * against malformation or corruption.
 * Uses argument ranges defined in cmd_dict json.
 */

#ifndef DSESW_CMD_VALIDATION_H
#define DSESW_CMD_VALIDATION_H

#include "cmn.h"
#include "UART.h"
#include "eep.h"
#include "actions.h"

CmnStatus cmd_validate_cmd_no_op(const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_cmd_restrict_override(const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_sys_config(U8& i2c_addr, U8& secs_to_boot, U8& behavior_interval, const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_actions_run_action(ActionTypes& action_type, U8& action_arg, const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_eep_peek(U16& address, const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_eep_poke(U16& address, U8& value, const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_eep_dump_record(EEP_RECORDS& eep_record, const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_behaviors_run_behavior(U8& behavior_id, const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_behaviors_abort(const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_behaviors_create_behavior(U8& behavior_id, const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_behaviors_create_add_action(ActionTypes& action_type, U8& action_arg, const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_behaviors_create_commit_behavior(const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_behaviors_create_cancel(const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_servo_move(U8& servo_id, U8& position, const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_servo_open(U8& servo_id, const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_servo_close(U8& servo_id, const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_servo_set_softstops(U8& servo_id, U8& low_softstop, U8& high_softstop, const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_servo_set_open_pos(U8& servo_id, U8& open_pos, const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_servo_set_close_pos(U8& servo_id, U8& closed_pos, const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_servo_save_config(const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_servo_dump_config(const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_servo_stop(U8& servo_id, const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_servo_stop_all(const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_servo_abort_all(const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_eep_wipe_record(EEP_RECORDS& eep_record, const UARTMsgBufferEntry& cmd_data);
CmnStatus cmd_validate_cmd_echo(char* string, U8& len, const UARTMsgBufferEntry& cmd_data);

CmnStatus cmd_validate_opcode(U8 opcode);

#endif
