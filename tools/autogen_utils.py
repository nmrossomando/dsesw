#!/usr/bin/env python

def gen_top_boilerplate(app_name):
    cmt = "/* {}\n".format(app_name)
    cmt += " * AUTO-GENERATED CODE. DO NOT EDIT.\n"
    cmt += " * \n"
    cmt += " * Built By FluidSW Code Generation Tools\n"
    cmt += " */\n"
    return cmt

def gen_include_guard_top(file_moniker):
    macro = "#ifndef DSESW_{}_H\n".format(file_moniker.upper())
    macro += "#define DSESW_{}_H\n".format(file_moniker.upper())
    return macro

def gen_include_guard_bottom():
    return "#endif\n"

def get_arg_bytes(arg):
    if arg["type"] in ["U8", "LOGIC", "ActionTypes", "EEP_RECORDS"]:
        return 1
    elif arg["type"] in ["U16"]:
        return 2
    elif arg["type"] in ["U32"]:
        return 4
    elif arg["type"] in ["STRING"]:
        return arg["maxlen"]
    else:
        # TODO: This doesn't support ARRAY...need to fix that...
        raise ValueError("Type {} not supported by this autogen tool!".format(arg["type"]))

def validate_cdict(cdict):
    # Check for valid opcode length:
    if cdict["opcode_len"] not in [1,2,4]:
        raise ValueError("Opcode length {} was not in allowed set of [1,2,4]!".format(cdict["opcode_len"]))

    # Check if all names in cdict are unique:
    names = [x["name"] for x in cdict["commands"]]
    for name in names:
        if names.count(name) > 1:
            raise ValueError("Command {} appears more than once in the provided cmd dictionary.".format(name))

    # Check if all opcodes in cdict are unique:
    opcodes = [x["opcode"] for x in cdict["commands"]]
    for opcode in opcodes:
        if opcodes.count(opcode) > 1:
            raise ValueError("Opcode {} appears more than once in the provided cmd dictionary.".format(opcode))
    
    # Check if all commands fit in the 4-byte argument limit (DSESW specific, need to generalize.)
    for cmd in cdict["commands"]:
        total_arg_bytes = 0
        for arg in cmd["args"]:
            total_arg_bytes += get_arg_bytes(arg)
        if total_arg_bytes > 4:
            raise ValueError("Command {} has {} bytes of arguments, but max is 4!".format(cmd["name"], total_arg_bytes))

    # Check if custom types have unique value mappings:
    types = [x for x in cdict["types"]]
    for t in types:
        if types.count(t) > 1:
            raise ValueError("Type {} occurs more than once in type defs of cmd_dict!".format(t))

    for t in cdict["types"]:
        type_names = [x for x in cdict["types"][t]]
        for name in type_names:
            if type_names.count(name) > 1:
                raise ValueError("Type {} has entry {} more than once!".format(t, name))
        
        type_vals = [cdict["types"][t][x] for x in cdict["types"][t]]
        for val in type_vals:
            if type_vals.count(val) > 1:
                raise ValueError("Type {} has value {} more than once!".format(t, val))

    #TODO: More validation

    return # If we don't raise an exception, it passed.
