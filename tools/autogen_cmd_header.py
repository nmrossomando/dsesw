#!/usr/bin/env python
# Auto generates cmd header.
# Because something's gotta give with the drudgery.
#
# This is driven by the cmd_dict, and generates FluidSW header
# for the command handling software. This is mostly to auto-gen
# the Cmd enum, which would be annoying otherwise.

import json
import argparse

import autogen_utils

def gen_cmdhead_boilerplate():
    cmt = "/*\n"
    cmt += " * Command header file.\n"
    cmt += " * Auto gen header to ease creation of the big\n"
    cmt += " * Cmd enum that's used internally.\n"
    cmt += " * Uses names and opcodes defined in cmd_dict json.\n"
    cmt += " */\n"
    return cmt

def gen_cmd_enum(cdict):
    opcode_len = cdict["opcode_len"]
    if opcode_len == 1:
        opcode_type = "U8"
    elif opcode_len == 2:
        opcode_type = "U16"
    else:
        opcode_type = "U32"

    code = "enum class Cmds : {} {{\n".format(opcode_type)
    for cmd in cdict["commands"]:
        code += "    {} = {},\n".format(cmd["name"], cmd["opcode"])
    # remove last comma...
    code = code[:-2]
    code += "\n"
    code += "};\n"

    return code

def gen_boilerplate_prototypes():
    code = "void cmd_init();\n"
    code += "void cmd_main();\n"
    code += "void cmd_dispatch_cmd(const UARTMsgBufferEntry& cmd_data);\n"
    return code

def gen_header(cdict):
    autogen_utils.validate_cdict(cdict)

    with open("cmd.h", "w") as f:
        f.write(autogen_utils.gen_top_boilerplate(cdict["app_name"]))
        f.write("\n")
        f.write(gen_cmdhead_boilerplate())
        f.write("\n")
        f.write(autogen_utils.gen_include_guard_top("cmd"))
        f.write("\n")
        f.write('#include "cmn.h"\n')
        f.write('#include "UART.h"\n')
        f.write("\n")
        f.write(gen_cmd_enum(cdict))
        f.write("\n")
        f.write(gen_boilerplate_prototypes())
        f.write("\n")
        f.write(autogen_utils.gen_include_guard_bottom())

    return

def main():
    parser = argparse.ArgumentParser(prog="autogen_cmd_header", description="Auto generates fluidSW cmd header file.")
    parser.add_argument("cmd_dict", help="path to a fluidSW json cmd dictionary")
    args = parser.parse_args()

    with open(args.cmd_dict, "r") as f:
        cdict = json.load(f)

    gen_header(cdict)

if __name__ == "__main__":
    main()
