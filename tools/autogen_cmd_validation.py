#!/usr/bin/env python
# Auto generates cmd validation check functions.
# Because something's gotta give with the drudgery.
#
# This is driven by the cmd_dict, and generates FluidSW functions
# to validate incoming commands on board the target device.
#
# This helps protect against either malformed commands coming from a source
# that doesn't do great validation on its own, or against data corruption.

import json
import argparse

import autogen_utils

def gen_cmdval_boilerplate():
    cmt = "/*\n"
    cmt += " * Command validation functions.\n"
    cmt += " * Sanity check incoming command arguments to guard\n"
    cmt += " * against malformation or corruption.\n"
    cmt += " * Uses argument ranges defined in cmd_dict json.\n"
    cmt += " */\n"
    return cmt

def gen_cmdval_prototypes(cdict):
    prototypes = ""
    for cmd in cdict["commands"]:
        proto = "CmnStatus cmd_validate_{}(".format(cmd["name"].lower())
        for arg in cmd["args"]:
            if(arg["type"] == "STRING"):
                proto += "char* {}, ".format(arg["name"]) # This kinda can't deal with generalized string cases, but neither can the compiler. If your string is not the last argument you're in trouble. Fix this...
                proto += "U8& len, "
            else:
                proto += arg["type"] # This will not generate valid code for "ARRAY" or "LOGIC" types that the cmd compiler supports. DSESW doesn't use any though.
                proto += "& {}, ".format(arg["name"])
        proto += "const UARTMsgBufferEntry& cmd_data);\n"
        prototypes += proto

    prototypes += "\n"
    prototypes += "CmnStatus cmd_validate_opcode(U8 opcode);\n"

    return prototypes

# This only supports types that are in the DSESW dict at time of writing...
# In future need to support signed ints, floats, arrays, and better support generic types...
def gen_cmdval_func(cmd, types):
    func = "CmnStatus cmd_validate_{}(".format(cmd["name"].lower())
    for arg in cmd["args"]:
        if(arg["type"] == "STRING"):
            func += "char* {}, ".format(arg["name"]) # This kinda can't deal with generalized string cases, but neither can the compiler. If your string is not the last argument you're in trouble. Fix this...
            func += "U8& len, "
        else:
            func += arg["type"] # This will not generate valid code for "ARRAY" or "LOGIC" types that the cmd compiler supports. DSESW doesn't use any though.
            func += "& {}, ".format(arg["name"])
    func += "const UARTMsgBufferEntry& cmd_data) {\n"
    
    idx = 0
    for arg in cmd["args"]:
        arg_valid = "    // Validate {}:\n".format(arg["name"])
        if arg["type"] == "STRING":
            str_idx = 0
            arg_valid += "    char tmp;\n"
            while idx < 4:
                arg_valid += "    tmp = cmd_data.args[{}];\n".format(idx)
                arg_valid += "    if(tmp == 0) {\n"
                arg_valid += "        {}[{}] = 0;\n".format(arg["name"], str_idx)
                arg_valid += "        len = {};\n".format(str_idx)
                arg_valid += "        return CmnStatus::SUCCESS;\n"
                arg_valid += "    }\n"
                arg_valid += "    else if((tmp < 32) || (tmp > 126)) {\n"
                arg_valid += "        return CmnStatus::FAILURE;\n"
                arg_valid += "    }\n"
                arg_valid += "    {}[{}] = tmp;\n".format(arg["name"], str_idx)
                str_idx += 1
                idx += 1
            arg_valid += "    len = {};\n".format(str_idx)
            arg_valid += "\n"
        else:
            arg_bytes = autogen_utils.get_arg_bytes(arg)
            if arg_bytes == 1:
                arg_valid += "    U8 {}_tmp = cmd_data.args[{}]; \n".format(arg["name"], idx)
                if arg["type"] == "U8":
                    arg_valid += "    if(({}_tmp < {}) || ({}_tmp > {})) {{\n".format(arg["name"], arg["min"], arg["name"], arg["max"])
                    arg_valid += "        return CmnStatus::FAILURE;\n"
                    arg_valid += "    }\n"
                    arg_valid += "    {} = {}_tmp;\n".format(arg["name"], arg["name"])
                else:
                    arg_valid += gen_type_validation(arg, types)
                arg_valid += "\n"
                idx += 1
            elif arg_bytes == 2:
                # Commands are sent big-endian to comply with CCSDS Space Packet protocol...which isn't used in DSESW, but the cmd compiler is meant to support generically.
                # AVR is little endian (in terms of data stoarge), so we need to byteswap.
                arg_valid += "    U16 {}_tmp = (cmd_data.args[{}] << 8) | (cmd_data.args[{}]);\n".format(arg["name"], idx, idx+1)
                arg_valid += "    if(({}_tmp < {}) || ({}_tmp > {})) {{\n".format(arg["name"], arg["min"], arg["name"], arg["max"])
                arg_valid += "        return CmnStatus::FAILURE;\n"
                arg_valid += "    }\n"
                arg_valid += "    {} = {}_tmp;\n".format(arg["name"], arg["name"])
                arg_valid += "\n"
                idx += 2
            elif arg_bytes == 4:
                # Commands are sent big-endian to comply with CCSDS Space Packet protocol...which isn't used in DSESW, but the cmd compiler is meant to support generically.
                # AVR is little endian (in terms of data stoarge), so we need to byteswap.
                arg_valid += "    U32 {}_tmp = (cmd_data.args[{}] << 24) | (cmd_data.args[{}]) << 16 | (cmd_data.args[{}] << 8) | (cmd_data.args[{}]);\n".format(arg["name"], idx, idx+1, idx+2, idx+3)
                arg_valid += "    if(({}_tmp < {}) || ({}_tmp > {})) {{\n".format(arg["name"], arg["min"], arg["name"], arg["max"])
                arg_valid += "        return CmnStatus::FAILURE;\n"
                arg_valid += "    }\n"
                arg_valid += "    {} = {}_tmp;\n".format(arg["name"], arg["name"])
                arg_valid += "\n"
                idx += 4
        func += arg_valid

    # Validate that all non used args in the struct are zero:
    while idx < 4:
        func += "    if(cmd_data.args[{}] != 0) {{\n".format(idx)
        func += "        return CmnStatus::FAILURE;\n"
        func += "    }\n"
        func += "\n"
        idx += 1
    
    func += "    return CmnStatus::SUCCESS;\n"
    func += "}\n"

    return func

def gen_type_validation(arg, types):
    type_def = types[arg["type"]]
    # TODO: Genericize this, instead of hardcoding..........
    if arg["type"] == "ActionTypes":
        # Idea is action type will be continous except for NONE, so get the range of vals and tack on a NONE check.
        arg_valid_values = [type_def[x] for x in type_def]
        arg_valid_values.remove(255) # NONE
        arg_valid = "    if(({}_tmp < {}) || ({}_tmp > {})) {{\n".format(arg["name"], min(arg_valid_values), arg["name"], max(arg_valid_values))
        arg_valid += "        if({}_tmp != 255) {{\n".format(arg["name"])
        arg_valid += "            return CmnStatus::FAILURE;\n"
        arg_valid += "        }\n"
        arg_valid += "    }\n"
        arg_valid += "    {} = static_cast<ActionTypes>({}_tmp);\n".format(arg["name"], arg["name"])
        return arg_valid
    elif arg["type"] == "EEP_RECORDS":
        # EEP_RECORDS is set in stone in terms of values so can actually do a simple min-max check.
        arg_valid = "    if(({}_tmp < {}) || ({}_tmp > {})) {{\n".format(arg["name"], 0, arg["name"], 31)
        arg_valid += "        return CmnStatus::FAILURE;\n"
        arg_valid += "    }\n"
        arg_valid += "    {} = static_cast<EEP_RECORDS>({}_tmp);\n".format(arg["name"], arg["name"])
        return arg_valid
    else:
        raise ValueError("Argument type {} not supported in DSESW!".format(arg["type"]))
    
def gen_opcode_validation(cdict):
    func = "CmnStatus cmd_validate_opcode(U8 opcode) {\n"

    opcodes = [x["opcode"] for x in cdict["commands"]]
    func += "    U8 valid_opcodes[{}] = {{".format(len(opcodes))
    for opcode in opcodes:
        func += "{},".format(opcode)
    func = func[:-1] # Slice off last comma.
    func += "};\n"

    func += "    for(auto oc : valid_opcodes) {\n"
    func += "        if(oc == opcode) {\n"
    func += "            return CmnStatus::SUCCESS;\n"
    func += "        }\n"
    func += "    }\n"
    func += "    return CmnStatus::FAILURE;\n"
    func += "}\n"

    return func

def gen_header(cdict):
    with open("cmd_validation.h", "w") as f:
        f.write(autogen_utils.gen_top_boilerplate(cdict["app_name"]))
        f.write("\n")
        f.write(gen_cmdval_boilerplate())
        f.write("\n")
        f.write(autogen_utils.gen_include_guard_top("cmd_validation"))
        f.write("\n")
        f.write('#include "cmn.h"\n')
        f.write('#include "UART.h"\n')
        f.write('#include "eep.h"\n') # Custom for DSESW to support custom type (TODO: automate...)
        f.write('#include "actions.h"\n') # Custom for DSESW to support custom type (TODO: automate...)
        f.write("\n")
        f.write(gen_cmdval_prototypes(cdict))
        f.write("\n")
        f.write(autogen_utils.gen_include_guard_bottom())

    return

def gen_source(cdict):
    with open("cmd_validation.cpp", "w") as f:
        f.write(autogen_utils.gen_top_boilerplate(cdict["app_name"]))
        f.write("\n")
        f.write(gen_cmdval_boilerplate())
        f.write("\n")
        f.write('#include "cmd_validation.h"\n')
        f.write('#include "cmd.h"\n')
        f.write("\n")
        for cmd in cdict["commands"]:
            f.write(gen_cmdval_func(cmd, cdict["types"]))
            f.write("\n")
        f.write(gen_opcode_validation(cdict))
        f.write("\n")

    return

def main():
    parser = argparse.ArgumentParser(prog="autogen_cmd_validation", description="Auto generates fluidSW cmd validation check functions.")
    parser.add_argument("cmd_dict", help="path to a fluidSW json cmd dictionary")
    args = parser.parse_args()

    with open(args.cmd_dict, "r") as f:
        cdict = json.load(f)

    autogen_utils.validate_cdict(cdict)

    gen_header(cdict)
    gen_source(cdict)

if __name__ == "__main__":
    main()
