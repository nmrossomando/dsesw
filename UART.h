#ifndef DSESW_UART_H
#define DSESW_UART_H

#include "cmn.h"

enum class UARTStatus : U8
{
    SUCCESS,
    FAILURE
};

// From cmd dict:
// Opcode is a U8,
// Longest arg list is 4 bytes.
// RCV function will only write to empty entries. (Therefore, will drop if there's an overrun)
// UART init will empty all entries.
// CMD processing will empty the entries it processes.
struct UARTMsgBufferEntry
{
    U8 opcode;
    U8 args[4];
};

void uart_init();                                     // 9600 8N1 for this use case.
UARTStatus uart_xmit(U8 *data, U8 datalen); // Will assume data is already packetized.
UARTStatus uart_process_rcv_buffer();
UARTStatus uart_get_msg_buffer_entry(UARTMsgBufferEntry &entry); // will return the oldest waiting message in the queue.

#endif
