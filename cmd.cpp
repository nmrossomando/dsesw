#include "cmd.h"
#include "cmd_validation.h"

#include "tlm.h"
#include "eep.h"
#include "actions.h"
#include "behaviors.h"
#include "servoctl.h"

bool cmd_restrictions_overriden;
Cmds restricted_cmds[2];

CmnStatus cmd_run_no_op();
CmnStatus cmd_run_echo(char* string, U8 len);
CmnStatus cmd_restrict_override();
void cmd_validation_failure_log_maker(const UARTMsgBufferEntry& cmd_data);

void cmd_init() {
    cmd_restrictions_overriden = false;
    restricted_cmds[0] = Cmds::EEP_POKE;
    restricted_cmds[1] = Cmds::EEP_WIPE_RECORD;
}

void cmd_main() {
    // Trigger UART receive buffer processing:
    uart_process_rcv_buffer(); // Don't bother with status here, because we may have queued commands without receiving anything new.

    // Get the next command from the UART message buffer:
    UARTMsgBufferEntry msg;
    UARTStatus stat = uart_get_msg_buffer_entry(msg);
    if(stat == UARTStatus::FAILURE) {
        return; // No commands to execute, no fun!
    }

    // If we do get a message back from UART, execute the command!
    cmd_dispatch_cmd(msg);
}

void cmd_dispatch_cmd(const UARTMsgBufferEntry& cmd_data) {
    // Check for valid opcode. If valid, we can safely continue:
    CmnStatus val_stat = cmd_validate_opcode(cmd_data.opcode);
    if(val_stat == CmnStatus::FAILURE) {
        TLM_LOG_MESSAGE(LogLevels::WARNING_HI, LogMessages::CMD_LOG_BAD_OPCODE, "Rejecting command with unknown opcode %u", cmd_data.opcode);
        return;
    }

    // Check if restricted. If yes, die unless overridden. If overridden, un-override.
    if(!cmd_restrictions_overriden) {
        for(auto c : restricted_cmds) {
            if(cmd_data.opcode == static_cast<U8>(c)) {
                TLM_LOG_MESSAGE(LogLevels::WARNING_LO, LogMessages::CMD_LOG_DISPATCH_RESTRICTED, "Failed to dispatched command %s because it is restricted!", cmd_data.opcode);
                return;
            }
        }
    }
    cmd_restrictions_overriden = false;

    // There are surely better ways to do this, but this was most expedient for the DSESW use case. :/
    TLM_LOG_MESSAGE(LogLevels::COMMAND, LogMessages::CMD_LOG_DISPATCHING_COMMAND, "Dispatching command %s.", cmd_data.opcode);
    Cmds cmd = static_cast<Cmds>(cmd_data.opcode);
    CmnStatus stat;
    switch(cmd)
    {
        case Cmds::CMD_NO_OP:
            stat = cmd_validate_cmd_no_op(cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = cmd_run_no_op();
            break;
        case Cmds::CMD_RESTRICT_OVERRIDE:
            stat = cmd_validate_cmd_restrict_override(cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = cmd_restrict_override();
            break;
        case Cmds::SYS_CONFIG:
            U8 i2c_addr;
            U8 secs_to_boot;
            U8 behavior_interval;
            stat = cmd_validate_sys_config(i2c_addr, secs_to_boot, behavior_interval, cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            // This is sneaky but I didn't build in a way to change these in advance...
            // Annnnd also any of these changes should require a boot cycle anyway.
            EepRecord cfg;
            cfg.data[0] = i2c_addr;
            cfg.data[1] = secs_to_boot;
            cfg.data[2] = behavior_interval;
            stat = eep_write_record(EEP_RECORDS::SYSTEM_CONFIGURATION, cfg);
            break;
        case Cmds::ACTIONS_RUN_ACTION:
            Action run_action;
            ActionTypes run_action_type;
            U8 run_arg;
            stat = cmd_validate_actions_run_action(run_action_type, run_arg, cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            run_action.id = run_action_type;
            run_action.arg = run_arg;
            stat = actions_execute_action(run_action);
            break;
        case Cmds::EEP_PEEK:
            U16 peek_addr;
            stat = cmd_validate_eep_peek(peek_addr, cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = eep_peek(peek_addr);
            break;
        case Cmds::EEP_POKE:
            U16 poke_addr;
            U8 poke_val;
            stat = cmd_validate_eep_poke(poke_addr, poke_val, cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = eep_poke(poke_addr, poke_val);
            break;
        case Cmds::EEP_DUMP_RECORD:
            EEP_RECORDS dump_record;
            stat = cmd_validate_eep_dump_record(dump_record, cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = eep_dump_record(dump_record);
            break;
        case Cmds::BEHAVIORS_RUN_BEHAVIOR:
            U8 run_behavior_id;
            stat = cmd_validate_behaviors_run_behavior(run_behavior_id, cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = behaviors_execute_behavior(run_behavior_id);
            break;
        case Cmds::BEHAVIORS_ABORT:
            stat = cmd_validate_behaviors_abort(cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = behaviors_stop_behavior(CmnStatus::FAILURE);
            break;
        case Cmds::BEHAVIORS_CREATE_BEHAVIOR:
            U8 create_behavior_id;
            stat = cmd_validate_behaviors_create_behavior(create_behavior_id, cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = behaviors_create_new_behavior(create_behavior_id);
            break;
        case Cmds::BEHAVIORS_CREATE_ADD_ACTION:
            ActionTypes add_action_action_type;
            U8 add_action_action_arg;
            stat = cmd_validate_behaviors_create_add_action(add_action_action_type, add_action_action_arg, cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = behaviors_new_behavior_add_action(add_action_action_type, add_action_action_arg);
            break;
        case Cmds::BEHAVIORS_CREATE_COMMIT_BEHAVIOR:
            stat = cmd_validate_behaviors_create_commit_behavior(cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = behaviors_commit_new_behavior();
            break;
        case Cmds::BEHAVIORS_CREATE_CANCEL:
            stat = cmd_validate_behaviors_create_cancel(cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = behaviors_create_cancel();
            break;
        case Cmds::SERVO_MOVE:
            U8 move_servo_id;
            U8 move_position;
            stat = cmd_validate_servo_move(move_servo_id, move_position, cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = servoctl_move_servo(move_servo_id, move_position);
            break;
        case Cmds::SERVO_OPEN:
            U8 open_servo_id;
            stat = cmd_validate_servo_open(open_servo_id, cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = servoctl_open_servo(open_servo_id);
            break;
        case Cmds::SERVO_CLOSE:
            U8 close_servo_id;
            stat = cmd_validate_servo_close(close_servo_id, cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = servoctl_close_servo(close_servo_id);
            break;
        case Cmds::SERVO_SET_SOFTSTOPS:
            U8 set_stop_servo_id;
            U8 set_stop_softstop_low;
            U8 set_stop_softstop_high;
            stat = cmd_validate_servo_set_softstops(set_stop_servo_id, set_stop_softstop_low, set_stop_softstop_high, cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = servoctl_set_softstops(set_stop_servo_id, set_stop_softstop_low, set_stop_softstop_high);
            break;
        case Cmds::SERVO_SET_OPEN_POS:
            U8 set_open_servo_id;
            U8 set_open_pos;
            stat = cmd_validate_servo_set_open_pos(set_open_servo_id, set_open_pos, cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = servoctl_set_open_pos(set_open_servo_id, set_open_pos);
            break;
        case Cmds::SERVO_SET_CLOSE_POS:
            U8 set_close_servo_id;
            U8 set_close_pos;
            stat = cmd_validate_servo_set_close_pos(set_close_servo_id, set_close_pos, cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = servoctl_set_close_pos(set_close_servo_id, set_close_pos);
            break;
        case Cmds::SERVO_SAVE_CONFIG:
            stat = cmd_validate_servo_save_config(cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = servoctl_save_config();
            break;
        case Cmds::SERVO_DUMP_CONFIG:
            stat = cmd_validate_servo_dump_config(cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            servoctl_dump_config();
            stat = CmnStatus::SUCCESS;
            break;
        case Cmds::SERVO_STOP:
            U8 stop_servo_id;
            stat = cmd_validate_servo_stop(stop_servo_id, cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = servoctl_stop_servo(stop_servo_id);
            break;
        case Cmds::SERVO_STOP_ALL:
            stat = cmd_validate_servo_stop_all(cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = servoctl_stop_all_servos();
            break;
        case Cmds::SERVO_ABORT_ALL:
            stat = cmd_validate_servo_abort_all(cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = servoctl_abort();
            break;
        case Cmds::EEP_WIPE_RECORD:
            EEP_RECORDS wipe_record;
            stat = cmd_validate_eep_wipe_record(wipe_record, cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = eep_wipe_record(wipe_record);
            break;
        case Cmds::CMD_ECHO:
            char string[4];
            U8 len;
            stat = cmd_validate_cmd_echo(string, len, cmd_data);
            if(stat == CmnStatus::FAILURE) {
                cmd_validation_failure_log_maker(cmd_data);
                break;
            }
            stat = cmd_run_echo(string, len);
            break;
    
        default:
            // This literally cannot happen, since we do opcode validation above.
            break;
    }

    if(stat == CmnStatus::FAILURE) {
        TLM_LOG_MESSAGE(LogLevels::COMMAND, LogMessages::CMD_LOG_COMPLETION_FAILURE, "Command %s completed with FAILURE!", static_cast<U8>(cmd));
    }
    else {
        TLM_LOG_MESSAGE(LogLevels::COMMAND, LogMessages::CMD_LOG_COMPLETION_SUCCESS, "Command %s completed with SUCCESS!", static_cast<U8>(cmd));
    }
}

CmnStatus cmd_run_no_op() {
    TLM_LOG_MESSAGE_NO_ARGS(LogLevels::ACTIVITY, LogMessages::CMD_LOG_EXECUTED_NO_OP, "Executed no-operation command.");
    return CmnStatus::SUCCESS;
}

CmnStatus cmd_run_echo(char* string, U8 len) {
    if((string[0] == 0) || (len == 0) || (len > 4)) {
        return CmnStatus::FAILURE;
    }

    // Why yes, this implementation sucks.
    // But our bounds are tight enough in DSESW to not need to both with proper string handling.
    U8 space = 32;
    if(len == 1) {
        TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::CMD_LOG_ECHO, "%c%c%c%c", string[0], space, space, space); // Spaces for the "extra" characters.
    }
    else if(len == 2) {
        TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::CMD_LOG_ECHO, "%c%c%c%c", string[0], string[1], space, space);
    }
    else if(len == 3) {
        TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::CMD_LOG_ECHO, "%c%c%c%c", string[0], string[1], string[2], space);
    }
    else if(len == 4) {
        TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::CMD_LOG_ECHO, "%c%c%c%c", string[0], string[1], string[2], string[3]);
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_restrict_override() {
    if(cmd_restrictions_overriden == true) {
        return CmnStatus::FAILURE;
    }
    TLM_LOG_MESSAGE_NO_ARGS(LogLevels::DIAGNOSTIC, LogMessages::CMD_LOG_RESTRICTIONS_LIFTED, "Lifted command restrictions for next command only!");
    cmd_restrictions_overriden = true;
    return CmnStatus::SUCCESS;
}

void cmd_validation_failure_log_maker(const UARTMsgBufferEntry& cmd_data) {
    TLM_LOG_MESSAGE(LogLevels::WARNING_LO, LogMessages::CMD_LOG_FAILED_VALIDATION, "Command %s with argument bytes %x, %x, %x, %x failed validation.", cmd_data.opcode, cmd_data.args[0], cmd_data.args[1], cmd_data.args[2], cmd_data.args[3]);
}
