#include "buscmd.h"

#include <Wire.h>
#include <Arduino.h>

#include "behaviors.h"
#include "actions.h"
#include "tlm.h"

enum class BuscmdBufferStatus : U8 {
    BUFFER_NORMAL,
    BUFFER_EMPTY,
    BUFFER_FULL
};

volatile U8 buscmd_buffer[16];
volatile U8 buscmd_buffer_read_pos;
volatile U8 buscmd_buffer_write_pos;

U8 startup_dur;
U8 spacing_dur;
U32 last_cmd_time;

void buscmd_handler(int bytes_rec);
BuscmdBufferStatus buscmd_buffer_read(U8& byte);
BuscmdBufferStatus buscmd_buffer_write(U8 byte);

void buscmd_init(U8 i2c_addr, U8 startup_ignore_dur, U8 cmd_spacing_dur) {
    Wire.begin(i2c_addr);
    startup_dur = startup_ignore_dur;
    spacing_dur = cmd_spacing_dur;

    buscmd_buffer_read_pos = 0;
    buscmd_buffer_write_pos = 0;
    for(auto& i : buscmd_buffer) {
        i = 0;
    }

    last_cmd_time = 0;

    Wire.onReceive(buscmd_handler);
}

void buscmd_main() {
    // Don't execute behaviors too close to each other, or overlapping:
    U32 next_valid_time = last_cmd_time + (spacing_dur * 1000);
    if(millis() < next_valid_time) {
        return;
    }
    else if(behaviors_is_behavior_executing()) {
        return;
    }

    // Discard commands recieved before startup is done:
    if(millis() < (static_cast<U32>(startup_dur) * 1000)) {
        BuscmdBufferStatus stat = BuscmdBufferStatus::BUFFER_NORMAL;
        while (stat != BuscmdBufferStatus::BUFFER_EMPTY) {
            U8 discard;
            stat = buscmd_buffer_read(discard);
        }
        return;
    }

    // Otherwise, execute a command!
    U8 next_cmd;
    BuscmdBufferStatus stat = buscmd_buffer_read(next_cmd);
    if(stat == BuscmdBufferStatus::BUFFER_EMPTY) {
        return; // No commands pending
    }
    
    TLM_LOG_MESSAGE(LogLevels::COMMAND, LogMessages::BUSCMD_LOG_EXECUTING_COMMAND, "Executing I2C Command %u.", next_cmd);
    last_cmd_time = millis();
    CmnStatus cmd_status;
    // Special command handling: STOP_ALL and ABORT are ALWAYS available, not tied to a behavior, so that the "killswitch" is never lost.
    if(next_cmd == 255) {
        Action abort_cmd;
        abort_cmd.id = ActionTypes::SERVOS_ABORT;
        abort_cmd.arg = 0;
        cmd_status = actions_execute_action(abort_cmd);
    }
    else if(next_cmd == 254) {
        Action stop_all_cmd;
        stop_all_cmd.id = ActionTypes::SERVOS_STOP_ALL;
        stop_all_cmd.arg = 0;
        cmd_status = actions_execute_action(stop_all_cmd);
    }
    else {
        cmd_status = behaviors_execute_behavior(next_cmd);
    }
    if(cmd_status == CmnStatus::FAILURE) {
        TLM_LOG_MESSAGE(LogLevels::COMMAND, LogMessages::BUSCMD_LOG_CMD_FAILURE, "I2C Command %u Completed with FAILURE.", next_cmd);
        return;
    }
    TLM_LOG_MESSAGE(LogLevels::COMMAND, LogMessages::BUSCMD_LOG_CMD_SUCCESS, "I2C Command %u Completed with SUCCESS.", next_cmd);
}

// This is a sneaky Arduino ISR, so I want to keep it lean. However, if there's an issue I will log it.
void buscmd_handler(int bytes_rec) {
    for(U8 i=0; i<bytes_rec; i++) {
        U8 b = Wire.read();
        BuscmdBufferStatus stat = buscmd_buffer_write(b);

        if(stat == BuscmdBufferStatus::BUFFER_FULL) {
            TLM_LOG_MESSAGE(LogLevels::WARNING_LO, LogMessages::BUSCMD_LOG_BUFFER_FULL, "Discarding recieved I2C command %d because command buffer was full.", b);
        }
    }
}

BuscmdBufferStatus buscmd_buffer_read(U8& byte) {
    if(buscmd_buffer_read_pos == buscmd_buffer_write_pos) {
        return BuscmdBufferStatus::BUFFER_EMPTY;
    }

    byte = buscmd_buffer[buscmd_buffer_read_pos];
    buscmd_buffer_read_pos = (buscmd_buffer_read_pos + 1) % 16;
    return BuscmdBufferStatus::BUFFER_NORMAL;
}

BuscmdBufferStatus buscmd_buffer_write(U8 byte) {
    U8 next_idx = (buscmd_buffer_write_pos + 1) % 16;
    if(next_idx == buscmd_buffer_read_pos) {
        return BuscmdBufferStatus::BUFFER_FULL;
    }

    buscmd_buffer[buscmd_buffer_write_pos] = byte;
    buscmd_buffer_write_pos = next_idx;
    return BuscmdBufferStatus::BUFFER_NORMAL;
}

