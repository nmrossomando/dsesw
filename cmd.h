/* DSESW - Dome Servo Extender Software
 * AUTO-GENERATED CODE. DO NOT EDIT.
 * 
 * Built By FluidSW Code Generation Tools
 */

/*
 * Command header file.
 * Auto gen header to ease creation of the big
 * Cmd enum that's used internally.
 * Uses names and opcodes defined in cmd_dict json.
 */

#ifndef DSESW_CMD_H
#define DSESW_CMD_H

#include "cmn.h"
#include "UART.h"

enum class Cmds : U8 {
    CMD_NO_OP = 0,
    CMD_RESTRICT_OVERRIDE = 1,
    SYS_CONFIG = 2,
    ACTIONS_RUN_ACTION = 3,
    EEP_PEEK = 4,
    EEP_POKE = 5,
    EEP_DUMP_RECORD = 6,
    BEHAVIORS_RUN_BEHAVIOR = 7,
    BEHAVIORS_ABORT = 8,
    BEHAVIORS_CREATE_BEHAVIOR = 9,
    BEHAVIORS_CREATE_ADD_ACTION = 10,
    BEHAVIORS_CREATE_COMMIT_BEHAVIOR = 11,
    BEHAVIORS_CREATE_CANCEL = 12,
    SERVO_MOVE = 13,
    SERVO_OPEN = 14,
    SERVO_CLOSE = 15,
    SERVO_SET_SOFTSTOPS = 16,
    SERVO_SET_OPEN_POS = 17,
    SERVO_SET_CLOSE_POS = 18,
    SERVO_SAVE_CONFIG = 19,
    SERVO_DUMP_CONFIG = 20,
    SERVO_STOP = 21,
    SERVO_STOP_ALL = 22,
    SERVO_ABORT_ALL = 23,
    EEP_WIPE_RECORD = 24,
    CMD_ECHO = 25
};

void cmd_init();
void cmd_main();
void cmd_dispatch_cmd(const UARTMsgBufferEntry& cmd_data);

#endif
