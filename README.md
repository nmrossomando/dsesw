# Dome Servo Extender Software (DSESW)

## Getting Started:

The repo uses a submodule for the commanding terminal. Make sure to clone recursively:

`git clone --recurse-submodules git@gitlab.com:nmrossomando/dsesw.git` for ssh or

`git clone --recurse-submosules https://gitlab.com/nmrossomando/dsesw.git` for https

You will probably need to either `mv dsesw/ DSESW/` or `cd dsesw && mv dsesw.ino DSESW.ino`. I forgot that
git likes to lowercase everything and arduino really wants the sketch and its parent folder to have
identical names, including case sensitivity...

Upload the sketch like any arduino sketch.

There are some python requirements for the command terminal, dometerm; there's a `requirements.txt` in 
`dometerm/fluidcdhterm/` that you can `pip install -r requirements.txt` to get all the requirements. (Tk 
is also required and IDK if that needs anything special on Mac. On Arch I needed to do `sudo pacman -S tk`).


## Using the command terminal

To use the command terminal, from the root directory of the repo (i.e. `dsesw/`), run `dometerm/dometerm`. At the top of the terminal
window that pops up, you can set up the serial port and load command and telemetry dictionaries. All of
those are populated to sensible defaults - the dictionary paths are valid as long as you started dometerm
from the root of the repo. The serial is 19200 baud, 8N1, so that's all valid too. The only thing you might
need to change is the serial port name. For me on Linux, with both FTDI and my CP2102 FTDI-alike thing,
it's always been `/dev/ttyUSB0` (or `/dev/ttyUSB1` if 0 was already taken.) I do not know what Mac will
use. I recommend loading dictionaries and _then_ opening the serial port, but it's not strictly necessary.
Opening the port **will reset the pro micro**.

The main window is pretty simple: The big central section shows log messages. The text entry bar lets you
send commands. The dropdown below that will populate the entry bar with a command's prototype if you want.
Type a command and hit enter to send.

The test entry bar also supports some sneaky "terminal commands":
- `~clear` will clear all log messages to clean uo the screen. (Once they're gone they're gone.)
- `~copy` will copy the entire log window to the clipboard.
- `~dump [filename]` will save the contents of the log window to `[filename]`.
- `~run [filename]` will run the list of commands in `[filename]` (locally, it will send them over UART one by one.)
- `~sleep [milliseconds]` is only usable in a "script" (a text file run by `~run`). It will pause execution of that file for `[milliseconds]`

An example script might be:
```
CMD_NO_OP
~sleep 1000
CMD_ECHO HI
~sleep 1000
SERVO_MOVE 5 180
```

The command terminal also pops up a second window called "DSESW Special Stuff!". It allows quicker access
to some DSESW commands. To use it, you must click "Load Dict Info" **after** loading dictionaries in the
main terminal window.

The left hand column of buttons is meant to stop stuff quickly if need be. "STOP EXECUTING BEHAVIOR" will
kill the running behavior, if any. "STOP SERVOS" should stop all servo motion immediately. If for some
reason it doesn't, "ABORT SERVOS" will ungracefully stop all servo motion - you'll need to reset the pro
micro to get the servos moving again.

The middle column lets you move and servo, run any action, or run any behavior. These are hopefully rather
self-explanatory.

The right column is supposed to have a "behavior builder" but I haven't had time to build it yet :(
(You can still build behaviors using raw commanding.)

## Using DSESW
DSESW interprets commands from two sources, UART and I2C. It sends log messages over UART. And it executes
"behaviors" made up of "actions".

### Commands
UART commanding and logging was largely covered in the above section. Here's the commands available:
- CMD_NO_OP
    - Do nothing, but send a log message
- CMD_RESTRICT_OVERRIDE
    - Allow the next command to be a restricted command. Restricted commands are EEP_POKE and EEP_WIPE_RECORD.
- SYS_CONFIG [i2c address] [seconds to boot] [behavior interval]
    - Sets the EEPROM record for system configuration. [i2c address] is the i2c address that will listen for commands. Must match what the stealth is sending to. [Seconds to boot] is how long to allow the stealth to boot before accepting commands - anything received before then is considered spurious. [behavior interval] is how many seconds to space out behavior execution by.
    - I used/baselined SYS_CONFIG 42 10 1 as defaults.
    - You must reboot the pro micro for these to take effect
- ACTIONS_RUN_ACTION [action_type] [arg]
    - Directly run an action of type [action type] with argument [arg].
    - [arg] is meaningless for some actions, but must still be provided.
- EEP_PEEK [address]
    - What it says on the tin
- EEP_POKE [address] [value]
    - What it says on the tin
    - Note: restricted command.
- EEP_DUMP_RECORD [Eep record id] 
    - Dump the entire contents of an EEPROM record via log messages.
- BEHAVIORS_RUN_BEHAVIOR [behavior id]
    - Run behavior with the given id. Valid ids are 0-28.
- BEHAVIORS_ABORT 
    - Kill any currently running behavior
- BEHAVIORS_CREATE_BEHAVIOR [behavior id]
    - Start the creation process of the given behavior id. Valid ids are 0-28.
    - This only initializes the behavior in RAM, nothing else. You must add actions to it, then save it to EEPROM, with further commands.
- BEHAVIORS_CREATE_ADD_ACTION [action type] [arg]
    - Add an action to the behavior currently being created.
    - Just like running an action, arg might be meaningless but must still be provided.
- BEHAVIORS_CREATE_COMMIT_BEHAVIOR
    - Save the behavior currently being created to EEPROM. This ends the behavior creation process.
- BEHAVIORS_CREATE_CANCEL
    - Discard the behavior currently being created instead of saving it.
- SERVO_MOVE [servo id] [position]
    - Move given servo to given position
    - servo id values are 2-13, matching the pin labelling on the servo extender.
    - position values are 0-180.
- SERVO_OPEN [servo id]
    - Move the given servo to its specified "open" position
    - servo id values are 2-13, matching the pin labelling on the servo extender.
- SERVO_CLOSE [servo id]
    - Move the given servo to its specified "open" position
    - servo id values are 2-13, matching the pin labelling on the servo extender.
- SERVO_SET_SOFTSTOPS [servo id] [low softstop] [high softstop]
    - Set the softstops for a given servo. Ids are 2-13, low stop must be less than high stop, stop values must be between 0 and 180.
    - Only affects RAM (volatile)
- SERVO_SET_OPEN_POS [servo id] [position]
    - Set the open position for a given servo. Ids are 2-13, values are 0-180
    - Only affects RAM (volatile)
- SERVO_SET_CLOSE_POS [servo id] [position]
    - Set the closed position for a given servo. Ids are 2-13, values are 0-180
    - Only affects RAM (volatile)
- SERVO_SAVE_CONFIG
    - Commit servo settings currently in RAM to EEPROM
- SERVO_DUMP_CONFIG
    - Dump the current servo config via log messages.
- SERVO_STOP [servo id]
    - Stop motion of a given servo, ids from 2-13.
- SERVO_STOP_ALL
    - Stop motion of all servos.
- SERVO_ABORT_ALL
    - Stop motion of all servos and **do not reenable them**.
- EEP_WIPE_RECORD [Eep record id]
    - Reset an EEPROM record to factory values (255).
    - Restricted command.
- CMD_ECHO [string]
    - Spit the given string back to the user as a log message.
    - 1-4 characters in the string.

### EEPROM Records
DSESW saves its configuration to the microcontroller's internal eeprom. There are 32 records:

- SYSTEM_CONFIGURATION contains the i2c address, boot time, and behavior interval
- SERVO_SOFTSTOPS contains servo softstop locations
- SERVO_NOMINAL_POSITIONS contains servo "opened" and "closed" positions
- BEHAVIOR_TABLE_X contains definitions for the behaviors tables (where X is 0-28)

### I2C commands
DSESW responds to the stealth's one-byte i2c commands. The commanding scheme is simple:
- If the byte contains a value from 0-28, DSESW executes that behavior id.
- If the byte contains 254, DSESW will stop all servos
- If the byte contains 255, DSESW will ABORT all servos (requires controller reset to get servos moving again).
- If any other byte, DSESW will try to execute a nonexistant behavior and fail to do so (bug...)

### Behaviors and Actions
Behaviors are DSESW's configurable system of responses to I2C commands. There are 29 available behavior "slots".
Each behavior may consist of up to 16 discrete actions.

Actions are the individual "steps" that a behavior may take. And action may optionally take a one-byte argument.
DSESW currently defines 19 possible actions:
- NOP: No operation, argument is ignored, but triggers a log message.
- WAIT: Pause behavior execution. Argument is time to wait in seconds.
- SERVO_OPEN: move a servo to its "open" position. Argument is servo id, 2-13
- SERVO_CLOSE: move a servo to its "closed" position. Argument is servo id, 2-13
- SERVO_X_MOVE: where "X" is a number 2-13 corresponding to a servo id. Argument is any position to move to, 0-180
- SERVO_STOP: stop a given servo, argument is servo id 2-13
- SERVOS_STOP_ALL: Stop all servos and then re-enable. Argument is ignored.
- SERVOS_ABORT: Stop all servos until microcontroller reset. Argument is ignored.

Behaviors are built via a series of UART commands. To start building a behavior, issue `BEHAVIORS_CREATE_BEHAVIOR`. Then add actions (in the order you want them to execute) using `BEHAVIORS_CREATE_ADD_ACTION`. Then save the behavior to EEPROM using `BEHAVIORS_CREATE_COMMIT_BEHAVIOR`. If you make a mistake or change your mind, you can cancel the process with `BEHAVIORS_CREATE_CANCEL`.
A sample behavior creation might look like:
```
BEHAVIORS_CREATE_BEHAVIOR 0
BEHAVIORS_CREATE_ADD_ACTION NOP 0
BEHAVIORS_CREATE_ADD_ACTION WAIT 5
BEHAVIORS_CREATE_ADD_ACTION SERVO_OPEN 6
BEHAVIORS_CREATE_ADD_ACTION WAIT 5
BEHAVIORS_CREATE_ADD_ACTION SERVO_CLOSE 6
BEHAVIORS_CREATE_COMMIT_BEHAVIOR
```
That will create behavior zero, which, when executed, will NOP, wait 5 seconds, open servo 6, wait 5 seconds, close servo 6.
