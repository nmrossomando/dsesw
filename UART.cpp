#include "UART.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include "cobs.h"

#include "tlm.h"

volatile UARTMsgBufferEntry msg_buffer[10]; // ...If we build up more than 10 queued commands we are trying way too hard.
volatile U8 msg_buffer_read_pos;
volatile U8 msg_buffer_write_pos;

volatile U8 rcv_buffer[30]; // Since we're going interrupt-driven for receive, we need this...
volatile U8 rcv_buffer_pos;
volatile bool rcv_pkt_complete;

void uart_init() {
    // Baud setting: 19.2k w/ U2X=0
    // Which per datasheet means 51 = 0b00110011
    // (Which means upper byte is 0!)
    UBRR0H = 0;
    UBRR0L = 51;

    // Control Register B: RX interrupt enable, RX enable, TX enable
    UCSR0B = (1 << RXCIE0) | (1 << RXEN0) | (1 << TXEN0);

    // CRs A and C are fine with their defaults.

    // Clear buffers:
    for(auto& i : msg_buffer) {
        i.opcode = 255;
        i.args[0] = 0;
        i.args[1] = 0;
        i.args[2] = 0;
        i.args[3] = 0;
    }
    msg_buffer_read_pos = 0;
    msg_buffer_write_pos = 0;

    for(auto& i : rcv_buffer) {
        i = 0;
    }
    rcv_buffer_pos = 0;
    rcv_pkt_complete = true;

    // Drive RX/TX high to start:
    PORTD |= (1 << PD0);
    PORTD |= (1 << PD1);
    sei();
}

ISR(USART_RX_vect) {
    // If the data buffer EVER rolls over, that means processing hasn't run in far too long (for this test app.)
    // So scream & return. (Also, since we're screaming, it's okay for the interrupt to burn more cycles here.)
    if(rcv_buffer_pos >= 30) {
        TLM_LOG_MESSAGE_NO_ARGS(LogLevels::WARNING_LO, LogMessages::UART_LOG_RX_OVERRUN, "Receive buffer overran! Data lost!");
        rcv_pkt_complete = true; // Not really but we'll deal with it later.
        return;
    }

    // Otherwise, do the thing as quickly as possible.
    // This means no error checking but for this app....eh.
    U8 data = UDR0;
    rcv_buffer[rcv_buffer_pos] = data;
    rcv_buffer_pos++;
    if(data == 0) {
        rcv_pkt_complete = true;
    }
    else {
        rcv_pkt_complete = false;
    }
}

UARTStatus uart_xmit(U8* data, U8 datalen) {
    if(data == nullptr) {
        TLM_LOG_MESSAGE_NO_ARGS(LogLevels::FATAL, LogMessages::UART_LOG_TX_BUFF_NULL, "UART transmission failed, data pointer to NULL!");
        return UARTStatus::FAILURE;
    }

    U8 i = 0;
    while(i < datalen) {
        while(!(UCSR0A & (1 << UDRE0)));
        UDR0 = data[i];
        i++;
    }

    return UARTStatus::SUCCESS;
}

// If there's anything in the receive buffer, cobs decode it and put it in the message buffer.
UARTStatus uart_process_rcv_buffer() {
    // If there's a packet still incoming, block until it finishes:
    while(rcv_pkt_complete == false);

    // Kill interrupts, copy and clear buffer:
    U8 old_sreg = SREG;
    cli();
    U8 rcv_buf_copy[30];
    for(U8 i=0; i<30; i++) {
        rcv_buf_copy[i] = rcv_buffer[i];
        rcv_buffer[i] = 0;
    }
    rcv_buffer_pos = 0;
    SREG = old_sreg; // Reenable interrupts.

    // Find and decode packets iteratively:
    // Drop partials :shrug:
    U8 pkt_start_index = 0;
    U8 packets_found = 0;
    for(U8 i=0; i<30; i++) {
        if(rcv_buf_copy[i] == 0) {
            // If packet length is zero, stop processing, there are no more to find.
            // Else, process the packet and continue.
            U8 pkt_len = i - pkt_start_index;
            if(pkt_len == 0) {
                break;
            }
            
            U8 decode_buf[29]; // Hah, you can save a whole byte.
            cobs_decode_result stat = cobs_decode(decode_buf, 29, &rcv_buf_copy[pkt_start_index], pkt_len);

            if(stat.status != COBS_DECODE_OK) {
                // Warn and continue...
                TLM_LOG_MESSAGE(LogLevels::WARNING_LO, LogMessages::UART_LOG_COBS_DECODE_ERROR, "COBS Decode failed with status %s.", stat.status);
            }
            else if(stat.out_len > 5) {
                // Warn and continue...
                TLM_LOG_MESSAGE(LogLevels::WARNING_LO, LogMessages::UART_LOG_RX_PACKET_TOO_LONG, "Decoded packet of length %u which is longer than the max length of 5.", stat.out_len);
            }
            else {
                packets_found++;
                msg_buffer[msg_buffer_write_pos].opcode = decode_buf[0];

                msg_buffer[msg_buffer_write_pos].args[0] = 0;
                msg_buffer[msg_buffer_write_pos].args[1] = 0;
                msg_buffer[msg_buffer_write_pos].args[2] = 0;
                msg_buffer[msg_buffer_write_pos].args[3] = 0;
                for(U8 j=0; j<(stat.out_len-1); j++) {
                    msg_buffer[msg_buffer_write_pos].args[j] = decode_buf[j+1];
                }

                msg_buffer_write_pos += 1;
                if(msg_buffer_write_pos > 9) {
                    msg_buffer_write_pos = 0;
                }
            }

            pkt_start_index = i + 1;
        }
    }

    if(packets_found > 0) {
        TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::UART_LOG_RX_PROCESSED_PACKETS, "Processed %u packets from receive buffer.", packets_found);
        return UARTStatus::SUCCESS;
    }
    else {
        return UARTStatus::FAILURE;
    }
}

UARTStatus uart_get_msg_buffer_entry(UARTMsgBufferEntry& entry) {
    if(msg_buffer_read_pos == msg_buffer_write_pos) {
        return UARTStatus::FAILURE;
    }
    else if(msg_buffer[msg_buffer_read_pos].opcode == 255) {
        return UARTStatus::FAILURE;
    }
    else {
        entry.opcode = msg_buffer[msg_buffer_read_pos].opcode;
        entry.args[0] = msg_buffer[msg_buffer_read_pos].args[0];
        entry.args[1] = msg_buffer[msg_buffer_read_pos].args[1];
        entry.args[2] = msg_buffer[msg_buffer_read_pos].args[2];
        entry.args[3] = msg_buffer[msg_buffer_read_pos].args[3];

        msg_buffer[msg_buffer_read_pos].opcode = 255;
        msg_buffer_read_pos++;
        if(msg_buffer_read_pos > 9) {
            msg_buffer_read_pos = 0;
        }

        return UARTStatus::SUCCESS;
    }
}
