#include "actions.h"

#include <Arduino.h>

#include "tlm.h"
#include "behaviors.h"
#include "servoctl.h"

CmnStatus actions_execute_action(Action action) {
    // Are there better ways to do this? Yes, but I'm on a timeline here.
    TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::ACTIONS_LOG_RUNNING_ACTION, "Running action %s with argument %u.", action.id, action.arg);

    CmnStatus stat;

    switch(action.id) {
        case ActionTypes::NOP:
            TLM_LOG_MESSAGE_NO_ARGS(LogLevels::ACTIVITY, LogMessages::ACTIONS_LOG_NOP, "Executed no-op activity.");
            stat = CmnStatus::SUCCESS;
            break;
        case ActionTypes::SERVO_OPEN:
            stat = servoctl_open_servo(action.arg);
            break;
        case ActionTypes::SERVO_CLOSE:
            stat = servoctl_close_servo(action.arg);
            break;
        case ActionTypes::SERVO_2_MOVE:
            stat = servoctl_move_servo(2, action.arg);
            break;
        case ActionTypes::SERVO_3_MOVE:
            stat = servoctl_move_servo(3, action.arg);
            break;
        case ActionTypes::SERVO_4_MOVE:
            stat = servoctl_move_servo(4, action.arg);
            break;
        case ActionTypes::SERVO_5_MOVE:
            stat = servoctl_move_servo(5, action.arg);
            break;
        case ActionTypes::SERVO_6_MOVE:
            stat = servoctl_move_servo(6, action.arg);
            break;
        case ActionTypes::SERVO_7_MOVE:
            stat = servoctl_move_servo(7, action.arg);
            break;
        case ActionTypes::SERVO_8_MOVE:
            stat = servoctl_move_servo(8, action.arg);
            break;
        case ActionTypes::SERVO_9_MOVE:
            stat = servoctl_move_servo(9, action.arg);
            break;
        case ActionTypes::SERVO_10_MOVE:
            stat = servoctl_move_servo(10, action.arg);
            break;
        case ActionTypes::SERVO_11_MOVE:
            stat = servoctl_move_servo(11, action.arg);
            break;
        case ActionTypes::SERVO_12_MOVE:
            stat = servoctl_move_servo(12, action.arg);
            break;
        case ActionTypes::SERVO_13_MOVE:
            stat = servoctl_move_servo(13, action.arg);
            break;
        case ActionTypes::SERVO_STOP:
            stat = servoctl_stop_servo(action.arg);
            break;
        case ActionTypes::SERVOS_STOP_ALL:
            stat = servoctl_stop_all_servos();
            break;
        case ActionTypes::SERVOS_ABORT:
            stat = servoctl_abort();
            break;
        case ActionTypes::NONE:
            return CmnStatus::FAILURE; // I can't think of a case where this should happen but it's not strictly "wrong" so just...do nothing silently.
        default:
            TLM_LOG_MESSAGE(LogLevels::WARNING_HI, LogMessages::ACTIONS_LOG_UNDEFINED_ACTION, "Recieved undefined action id %u. Aborting.", static_cast<U8>(action.id));
            return CmnStatus::FAILURE;
        case ActionTypes::WAIT:
            U32 resume_time = millis() + (static_cast<U32>(action.arg) * 1000);
            behaviors_pause_behavior(resume_time);
            stat = CmnStatus::SUCCESS;
            break;
    }

    return stat;
}
