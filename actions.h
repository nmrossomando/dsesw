#ifndef DSESW_ACTIONS_H
#define DSESW_ACTIONS_H

#include "cmn.h"

enum class ActionTypes : U8 {
    NOP,
    WAIT,
    SERVO_OPEN,
    SERVO_CLOSE,
    SERVO_2_MOVE,
    SERVO_3_MOVE,
    SERVO_4_MOVE,
    SERVO_5_MOVE,
    SERVO_6_MOVE,
    SERVO_7_MOVE,
    SERVO_8_MOVE,
    SERVO_9_MOVE,
    SERVO_10_MOVE,
    SERVO_11_MOVE,
    SERVO_12_MOVE,
    SERVO_13_MOVE,
    SERVO_STOP,
    SERVOS_STOP_ALL,
    SERVOS_ABORT,
    NONE = 255
};

struct Action {
    ActionTypes id;
    U8 arg;
};

CmnStatus actions_execute_action(Action action);

#endif
