#include "servoctl.h"

#include <Servo.h>

#include "eep.h"
#include "tlm.h"

#define SERVO_IDX_TO_PIN(idx) idx+2
#define SERVO_PIN_TO_IDX(pin) pin-2

struct ServoSoftstops {
    U8 stop_high;
    U8 stop_low;
};
ServoSoftstops softstops[12];

struct ServoNominalPositions {
    U8 open_pos;
    U8 closed_pos;
};
ServoNominalPositions nominal_positions[12];

Servo servoctl_servos[12];

CmnStatus sanity_check_arg(U8 arg);
CmnStatus check_if_valid_servo(U8 servo_id);

void servoctl_init() {
    // Init all config to semi-sane values:
    for(auto& i : softstops) {
        i.stop_high = 180;
        i.stop_low = 0;
    }
    for(auto& i : nominal_positions) {
        i.open_pos = 180;
        i.closed_pos = 0;
    }

    // Then attempt to load config from EEPROM.
    EepRecord softstop_record;
    CmnStatus stat = eep_get_record(EEP_RECORDS::SERVO_SOFTSTOPS, softstop_record);
    if(stat == CmnStatus::FAILURE) {
        TLM_LOG_MESSAGE_NO_ARGS(LogLevels::WARNING_HI, LogMessages::SERVO_LOG_SOFTSTOP_EEP_LOAD_FAILED, "Failed to load softstop config from EEPROM! Using potentially unsafe defaults!");
    }
    else {
        // This is a heuristic, but hopefully a good one?
        if(softstop_record.data[0] == 255 && softstop_record.data[1] == 255) {
            TLM_LOG_MESSAGE_NO_ARGS(LogLevels::WARNING_HI, LogMessages::SERVO_LOG_SOFTSTOP_EEP_BLANK, "EEPROM softstop config was empty! Using potentially unsafe defaults!");
        }
        else {
            for(U8 i=0; i<24; i+=2) {
                softstops[i>>1].stop_high = softstop_record.data[i];
                softstops[i>>1].stop_low = softstop_record.data[i+1];
            }
        }
    }

    EepRecord position_record;
    stat = eep_get_record(EEP_RECORDS::SERVO_NOMINAL_POSITIONS, position_record);
    if(stat == CmnStatus::FAILURE) {
        TLM_LOG_MESSAGE_NO_ARGS(LogLevels::WARNING_HI, LogMessages::SERVO_LOG_POSITION_EEP_LOAD_FAILED, "Failed to load position config from EEPROM! Using potentially unsafe defaults!");
    }
    else {
        // This is a heuristic, but hopefully a good one?
        if(position_record.data[0] == 255 && position_record.data[1] == 255) {
            TLM_LOG_MESSAGE_NO_ARGS(LogLevels::WARNING_HI, LogMessages::SERVO_LOG_POSITION_EEP_BLANK, "EEPROM position config was empty! Using potentially unsafe defaults!");
        }
        else {
            for(U8 i=0; i<24; i+=2) {
                nominal_positions[i>>1].open_pos = position_record.data[i];
                nominal_positions[i>>1].closed_pos = position_record.data[i+1];
            }
        }
    }

    // Attach all servos. Maaaaybe not the most graceful but whatever.
    U8 k = 2;
    for(auto& i : servoctl_servos) {
        i.attach(k);
        k++;
    }

    // If servo close position is not 0 (i.e. uninitialized), default to closed:
    U8 servo_index = 0;
    for(auto& i : servoctl_servos) {
        if(nominal_positions[servo_index].closed_pos != 0) {
            i.write(nominal_positions[servo_index].closed_pos);
        }
        servo_index++;
    }
}

// Config:
CmnStatus servoctl_set_softstops(U8 servo_id, U8 stop_low, U8 stop_high) {
    U8 servo_index = SERVO_PIN_TO_IDX(servo_id);
    if((sanity_check_arg(stop_low) == CmnStatus::FAILURE) || (sanity_check_arg(stop_high) == CmnStatus::FAILURE)) {
        return CmnStatus::FAILURE;
    }
    else if(check_if_valid_servo(servo_id) == CmnStatus::FAILURE) {
        return CmnStatus::FAILURE;
    }
    
    softstops[servo_index].stop_high = stop_high;
    softstops[servo_index].stop_low = stop_low;
    TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::SERVO_LOG_SET_SOFTSTOPS, "Set servo %u softstops to high: %u, low: %u", servo_id, stop_high, stop_low);

    return CmnStatus::SUCCESS;
}

CmnStatus servoctl_set_open_pos(U8 servo_id, U8 position) {
    U8 servo_index = SERVO_PIN_TO_IDX(servo_id);
    if(sanity_check_arg(position) == CmnStatus::FAILURE) {
        return CmnStatus::FAILURE;
    }
    else if(check_if_valid_servo(servo_id) == CmnStatus::FAILURE) {
        return CmnStatus::FAILURE;
    }

    nominal_positions[servo_index].open_pos = position;
    TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::SERVO_LOG_SET_OPEN_POS, "Set servo %u open pos to: %u", servo_id, position);

    return CmnStatus::SUCCESS;
}

CmnStatus servoctl_set_close_pos(U8 servo_id, U8 position) {
    U8 servo_index = SERVO_PIN_TO_IDX(servo_id);
    if(sanity_check_arg(position) == CmnStatus::FAILURE) {
        return CmnStatus::FAILURE;
    }
    else if(check_if_valid_servo(servo_id) == CmnStatus::FAILURE) {
        return CmnStatus::FAILURE;
    }

    nominal_positions[servo_index].closed_pos = position;
    TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::SERVO_LOG_SET_CLOSED_POS, "Set servo %u closed pos to: %u", servo_id, position);

    return CmnStatus::SUCCESS;
}

CmnStatus servoctl_save_config() {
    EepRecord softstop_record;
    for(U8 i=0; i<24; i+=2) {
        softstop_record.data[i] = softstops[i>>1].stop_high;
        softstop_record.data[i+1] = softstops[i>>1].stop_low;
    }

    CmnStatus stat = eep_write_record(EEP_RECORDS::SERVO_SOFTSTOPS, softstop_record);
    if(stat == CmnStatus::FAILURE) {
        TLM_LOG_MESSAGE_NO_ARGS(LogLevels::WARNING_LO, LogMessages::SERVO_LOG_SAVE_SOFTSTOPS_FAILED, "Failed to save softstop configuration to EEPROM!");
        return CmnStatus::FAILURE;
    }

    EepRecord position_record;
    for(U8 i=0; i<24; i+=2) {
        position_record.data[i] = nominal_positions[i>>1].open_pos;
        position_record.data[i+1] = nominal_positions[i>>1].closed_pos;
    }

    stat = eep_write_record(EEP_RECORDS::SERVO_NOMINAL_POSITIONS, position_record);
    if(stat == CmnStatus::FAILURE) {
        TLM_LOG_MESSAGE_NO_ARGS(LogLevels::WARNING_LO, LogMessages::SERVO_LOG_SAVE_POSITIONS_FAILED, "Failed to save position configuration to EEPROM!");
        return CmnStatus::FAILURE;
    }

    TLM_LOG_MESSAGE_NO_ARGS(LogLevels::DIAGNOSTIC, LogMessages::SERVO_LOG_SAVED_CONFIG_TO_EEP, "Saved servo configuration to EEPROM.");
    return CmnStatus::SUCCESS;
}

void servoctl_dump_config() {
    for(U8 i=0; i<13; i++) {
        U8 servo_pin = SERVO_IDX_TO_PIN(i);
        TLM_LOG_MESSAGE(LogLevels::DIAGNOSTIC, LogMessages::SERVO_LOG_SHOW_CONFIG, "Servo %u: Softstop_high %u, Softstop low %u, Open position %u, Closed position %u", servo_pin, softstops[i].stop_high, softstops[i].stop_low, nominal_positions[i].open_pos, nominal_positions[i].closed_pos);
    }
}

// Servo motion:
CmnStatus servoctl_move_servo(U8 servo_id, U8 position) {
    U8 servo_index = SERVO_PIN_TO_IDX(servo_id);
    
    // Argument sanity check:
    CmnStatus stat = sanity_check_arg(position);
    if(stat == CmnStatus::FAILURE) {
        return CmnStatus::FAILURE;
    }
    else if(check_if_valid_servo(servo_id) == CmnStatus::FAILURE) {
        return CmnStatus::FAILURE;
    }

    // Position softstop check:
    if((position > softstops[servo_index].stop_high) || (position < softstops[servo_index].stop_low)) {
        TLM_LOG_MESSAGE(LogLevels::WARNING_LO, LogMessages::SERVO_LOG_REQUEST_VIOLATES_SOFTSTOP, "Servo %u cannot move to position %u because is is outside the softstop range %u-%u", servo_id, position, softstops[servo_index].stop_low, softstops[servo_index].stop_high);
        return CmnStatus::FAILURE;
    }

    // Servo move:
    TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::SERVO_LOG_MOVING_TO_POSITION, "Servo %u is moving to position %u.", servo_id, position);
    servoctl_servos[servo_index].write(position);
    return CmnStatus::SUCCESS;
}

CmnStatus servoctl_open_servo(U8 servo_id) {
    U8 servo_index = SERVO_PIN_TO_IDX(servo_id);
    if(check_if_valid_servo(servo_id) == CmnStatus::FAILURE) {
        return CmnStatus::FAILURE;
    }

    TLM_LOG_MESSAGE(LogLevels::DIAGNOSTIC, LogMessages::SERVO_LOG_MOVING_TO_OPEN_POS, "Moving servo %u to open position.", servo_id);
    return servoctl_move_servo(servo_id, nominal_positions[servo_index].open_pos);
}

CmnStatus servoctl_close_servo(U8 servo_id) {
    U8 servo_index = SERVO_PIN_TO_IDX(servo_id);
    if(check_if_valid_servo(servo_id) == CmnStatus::FAILURE) {
        return CmnStatus::FAILURE;
    }

    TLM_LOG_MESSAGE(LogLevels::DIAGNOSTIC, LogMessages::SERVO_LOG_MOVING_TO_CLOSED_POS, "Moving servo %u to closed position.", servo_id);
    return servoctl_move_servo(servo_id, nominal_positions[servo_index].closed_pos);
}

CmnStatus servoctl_stop_servo(U8 servo_id) {
    U8 servo_index = SERVO_PIN_TO_IDX(servo_id);
    if(check_if_valid_servo(servo_id) == CmnStatus::FAILURE) {
        return CmnStatus::FAILURE;
    }

    TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::SERVO_LOG_STOP_SERVO, "Stopping servo %u motion.", servo_id);
    // First stop given servo:
    servoctl_servos[servo_index].detach();
    // Then reattach it for further commands:
    servoctl_servos[servo_index].attach(servo_id);

    return CmnStatus::SUCCESS;
}

CmnStatus servoctl_stop_all_servos() {
    TLM_LOG_MESSAGE_NO_ARGS(LogLevels::ACTIVITY, LogMessages::SERVO_LOG_STOP_ALL_SERVOS, "Stopping all servo motion.");
    // First, detach all servos to stop motion...
    for(auto& i : servoctl_servos) {
        i.detach();
    }
    // Then reattach them all to receive further commands:
    U8 k = 2;
    for(auto& i : servoctl_servos) {
        i.attach(k);
        k++;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus servoctl_abort() {
    TLM_LOG_MESSAGE_NO_ARGS(LogLevels::WARNING_HI, LogMessages::SERVO_LOG_ABORT, "Aborting all servo motion! Servos will not be reeanabled!");
    for(auto& i : servoctl_servos) {
        i.detach();
    }
    return CmnStatus::SUCCESS;
}

CmnStatus sanity_check_arg(U8 arg) {
    if(arg > 180) {
        TLM_LOG_MESSAGE(LogLevels::WARNING_LO, LogMessages::SERVO_LOG_ARG_OUT_OF_RANGE, "Argument %u out of range 0-180 allowed for servos.", arg);
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus check_if_valid_servo(U8 servo_id) {
    if((servo_id < 2) || (servo_id > 13)) {
        TLM_LOG_MESSAGE(LogLevels::WARNING_LO, LogMessages::SERVO_LOG_INVALID_SERVO_ID, "Provided servo_id %u is out of available range 2-13.", servo_id);
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}
