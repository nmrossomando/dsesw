/* DSESW - Dome Servo Extender Software
 * AUTO-GENERATED CODE. DO NOT EDIT.
 * 
 * Built By FluidSW Code Generation Tools
 */

/*
 * Command validation functions.
 * Sanity check incoming command arguments to guard
 * against malformation or corruption.
 * Uses argument ranges defined in cmd_dict json.
 */

#include "cmd_validation.h"
#include "cmd.h"

CmnStatus cmd_validate_cmd_no_op(const UARTMsgBufferEntry& cmd_data) {
    if(cmd_data.args[0] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[1] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_cmd_restrict_override(const UARTMsgBufferEntry& cmd_data) {
    if(cmd_data.args[0] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[1] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_sys_config(U8& i2c_addr, U8& secs_to_boot, U8& behavior_interval, const UARTMsgBufferEntry& cmd_data) {
    // Validate i2c_addr:
    U8 i2c_addr_tmp = cmd_data.args[0]; 
    if((i2c_addr_tmp < 0) || (i2c_addr_tmp > 127)) {
        return CmnStatus::FAILURE;
    }
    i2c_addr = i2c_addr_tmp;

    // Validate secs_to_boot:
    U8 secs_to_boot_tmp = cmd_data.args[1]; 
    if((secs_to_boot_tmp < 0) || (secs_to_boot_tmp > 255)) {
        return CmnStatus::FAILURE;
    }
    secs_to_boot = secs_to_boot_tmp;

    // Validate behavior_interval:
    U8 behavior_interval_tmp = cmd_data.args[2]; 
    if((behavior_interval_tmp < 0) || (behavior_interval_tmp > 255)) {
        return CmnStatus::FAILURE;
    }
    behavior_interval = behavior_interval_tmp;

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_actions_run_action(ActionTypes& action_type, U8& action_arg, const UARTMsgBufferEntry& cmd_data) {
    // Validate action_type:
    U8 action_type_tmp = cmd_data.args[0]; 
    if((action_type_tmp < 0) || (action_type_tmp > 18)) {
        if(action_type_tmp != 255) {
            return CmnStatus::FAILURE;
        }
    }
    action_type = static_cast<ActionTypes>(action_type_tmp);

    // Validate action_arg:
    U8 action_arg_tmp = cmd_data.args[1]; 
    if((action_arg_tmp < 0) || (action_arg_tmp > 255)) {
        return CmnStatus::FAILURE;
    }
    action_arg = action_arg_tmp;

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_eep_peek(U16& address, const UARTMsgBufferEntry& cmd_data) {
    // Validate address:
    U16 address_tmp = (cmd_data.args[0] << 8) | (cmd_data.args[1]);
    if((address_tmp < 0) || (address_tmp > 1023)) {
        return CmnStatus::FAILURE;
    }
    address = address_tmp;

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_eep_poke(U16& address, U8& value, const UARTMsgBufferEntry& cmd_data) {
    // Validate address:
    U16 address_tmp = (cmd_data.args[0] << 8) | (cmd_data.args[1]);
    if((address_tmp < 0) || (address_tmp > 1023)) {
        return CmnStatus::FAILURE;
    }
    address = address_tmp;

    // Validate value:
    U8 value_tmp = cmd_data.args[2]; 
    if((value_tmp < 0) || (value_tmp > 255)) {
        return CmnStatus::FAILURE;
    }
    value = value_tmp;

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_eep_dump_record(EEP_RECORDS& eep_record, const UARTMsgBufferEntry& cmd_data) {
    // Validate eep_record:
    U8 eep_record_tmp = cmd_data.args[0]; 
    if((eep_record_tmp < 0) || (eep_record_tmp > 31)) {
        return CmnStatus::FAILURE;
    }
    eep_record = static_cast<EEP_RECORDS>(eep_record_tmp);

    if(cmd_data.args[1] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_behaviors_run_behavior(U8& behavior_id, const UARTMsgBufferEntry& cmd_data) {
    // Validate behavior_id:
    U8 behavior_id_tmp = cmd_data.args[0]; 
    if((behavior_id_tmp < 0) || (behavior_id_tmp > 28)) {
        return CmnStatus::FAILURE;
    }
    behavior_id = behavior_id_tmp;

    if(cmd_data.args[1] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_behaviors_abort(const UARTMsgBufferEntry& cmd_data) {
    if(cmd_data.args[0] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[1] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_behaviors_create_behavior(U8& behavior_id, const UARTMsgBufferEntry& cmd_data) {
    // Validate behavior_id:
    U8 behavior_id_tmp = cmd_data.args[0]; 
    if((behavior_id_tmp < 0) || (behavior_id_tmp > 28)) {
        return CmnStatus::FAILURE;
    }
    behavior_id = behavior_id_tmp;

    if(cmd_data.args[1] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_behaviors_create_add_action(ActionTypes& action_type, U8& action_arg, const UARTMsgBufferEntry& cmd_data) {
    // Validate action_type:
    U8 action_type_tmp = cmd_data.args[0]; 
    if((action_type_tmp < 0) || (action_type_tmp > 18)) {
        if(action_type_tmp != 255) {
            return CmnStatus::FAILURE;
        }
    }
    action_type = static_cast<ActionTypes>(action_type_tmp);

    // Validate action_arg:
    U8 action_arg_tmp = cmd_data.args[1]; 
    if((action_arg_tmp < 0) || (action_arg_tmp > 255)) {
        return CmnStatus::FAILURE;
    }
    action_arg = action_arg_tmp;

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_behaviors_create_commit_behavior(const UARTMsgBufferEntry& cmd_data) {
    if(cmd_data.args[0] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[1] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_behaviors_create_cancel(const UARTMsgBufferEntry& cmd_data) {
    if(cmd_data.args[0] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[1] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_servo_move(U8& servo_id, U8& position, const UARTMsgBufferEntry& cmd_data) {
    // Validate servo_id:
    U8 servo_id_tmp = cmd_data.args[0]; 
    if((servo_id_tmp < 2) || (servo_id_tmp > 13)) {
        return CmnStatus::FAILURE;
    }
    servo_id = servo_id_tmp;

    // Validate position:
    U8 position_tmp = cmd_data.args[1]; 
    if((position_tmp < 0) || (position_tmp > 180)) {
        return CmnStatus::FAILURE;
    }
    position = position_tmp;

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_servo_open(U8& servo_id, const UARTMsgBufferEntry& cmd_data) {
    // Validate servo_id:
    U8 servo_id_tmp = cmd_data.args[0]; 
    if((servo_id_tmp < 2) || (servo_id_tmp > 13)) {
        return CmnStatus::FAILURE;
    }
    servo_id = servo_id_tmp;

    if(cmd_data.args[1] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_servo_close(U8& servo_id, const UARTMsgBufferEntry& cmd_data) {
    // Validate servo_id:
    U8 servo_id_tmp = cmd_data.args[0]; 
    if((servo_id_tmp < 2) || (servo_id_tmp > 13)) {
        return CmnStatus::FAILURE;
    }
    servo_id = servo_id_tmp;

    if(cmd_data.args[1] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_servo_set_softstops(U8& servo_id, U8& low_softstop, U8& high_softstop, const UARTMsgBufferEntry& cmd_data) {
    // Validate servo_id:
    U8 servo_id_tmp = cmd_data.args[0]; 
    if((servo_id_tmp < 2) || (servo_id_tmp > 13)) {
        return CmnStatus::FAILURE;
    }
    servo_id = servo_id_tmp;

    // Validate low_softstop:
    U8 low_softstop_tmp = cmd_data.args[1]; 
    if((low_softstop_tmp < 0) || (low_softstop_tmp > 180)) {
        return CmnStatus::FAILURE;
    }
    low_softstop = low_softstop_tmp;

    // Validate high_softstop:
    U8 high_softstop_tmp = cmd_data.args[2]; 
    if((high_softstop_tmp < 0) || (high_softstop_tmp > 180)) {
        return CmnStatus::FAILURE;
    }
    high_softstop = high_softstop_tmp;

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_servo_set_open_pos(U8& servo_id, U8& open_pos, const UARTMsgBufferEntry& cmd_data) {
    // Validate servo_id:
    U8 servo_id_tmp = cmd_data.args[0]; 
    if((servo_id_tmp < 2) || (servo_id_tmp > 13)) {
        return CmnStatus::FAILURE;
    }
    servo_id = servo_id_tmp;

    // Validate open_pos:
    U8 open_pos_tmp = cmd_data.args[1]; 
    if((open_pos_tmp < 0) || (open_pos_tmp > 180)) {
        return CmnStatus::FAILURE;
    }
    open_pos = open_pos_tmp;

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_servo_set_close_pos(U8& servo_id, U8& closed_pos, const UARTMsgBufferEntry& cmd_data) {
    // Validate servo_id:
    U8 servo_id_tmp = cmd_data.args[0]; 
    if((servo_id_tmp < 2) || (servo_id_tmp > 13)) {
        return CmnStatus::FAILURE;
    }
    servo_id = servo_id_tmp;

    // Validate closed_pos:
    U8 closed_pos_tmp = cmd_data.args[1]; 
    if((closed_pos_tmp < 0) || (closed_pos_tmp > 180)) {
        return CmnStatus::FAILURE;
    }
    closed_pos = closed_pos_tmp;

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_servo_save_config(const UARTMsgBufferEntry& cmd_data) {
    if(cmd_data.args[0] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[1] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_servo_dump_config(const UARTMsgBufferEntry& cmd_data) {
    if(cmd_data.args[0] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[1] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_servo_stop(U8& servo_id, const UARTMsgBufferEntry& cmd_data) {
    // Validate servo_id:
    U8 servo_id_tmp = cmd_data.args[0]; 
    if((servo_id_tmp < 2) || (servo_id_tmp > 13)) {
        return CmnStatus::FAILURE;
    }
    servo_id = servo_id_tmp;

    if(cmd_data.args[1] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_servo_stop_all(const UARTMsgBufferEntry& cmd_data) {
    if(cmd_data.args[0] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[1] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_servo_abort_all(const UARTMsgBufferEntry& cmd_data) {
    if(cmd_data.args[0] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[1] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_eep_wipe_record(EEP_RECORDS& eep_record, const UARTMsgBufferEntry& cmd_data) {
    // Validate eep_record:
    U8 eep_record_tmp = cmd_data.args[0]; 
    if((eep_record_tmp < 0) || (eep_record_tmp > 31)) {
        return CmnStatus::FAILURE;
    }
    eep_record = static_cast<EEP_RECORDS>(eep_record_tmp);

    if(cmd_data.args[1] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[2] != 0) {
        return CmnStatus::FAILURE;
    }

    if(cmd_data.args[3] != 0) {
        return CmnStatus::FAILURE;
    }

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_cmd_echo(char* string, U8& len, const UARTMsgBufferEntry& cmd_data) {
    // Validate string:
    char tmp;
    tmp = cmd_data.args[0];
    if(tmp == 0) {
        string[0] = 0;
        len = 0;
        return CmnStatus::SUCCESS;
    }
    else if((tmp < 32) || (tmp > 126)) {
        return CmnStatus::FAILURE;
    }
    string[0] = tmp;
    tmp = cmd_data.args[1];
    if(tmp == 0) {
        string[1] = 0;
        len = 1;
        return CmnStatus::SUCCESS;
    }
    else if((tmp < 32) || (tmp > 126)) {
        return CmnStatus::FAILURE;
    }
    string[1] = tmp;
    tmp = cmd_data.args[2];
    if(tmp == 0) {
        string[2] = 0;
        len = 2;
        return CmnStatus::SUCCESS;
    }
    else if((tmp < 32) || (tmp > 126)) {
        return CmnStatus::FAILURE;
    }
    string[2] = tmp;
    tmp = cmd_data.args[3];
    if(tmp == 0) {
        string[3] = 0;
        len = 3;
        return CmnStatus::SUCCESS;
    }
    else if((tmp < 32) || (tmp > 126)) {
        return CmnStatus::FAILURE;
    }
    string[3] = tmp;
    len = 4;

    return CmnStatus::SUCCESS;
}

CmnStatus cmd_validate_opcode(U8 opcode) {
    U8 valid_opcodes[26] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25};
    for(auto oc : valid_opcodes) {
        if(oc == opcode) {
            return CmnStatus::SUCCESS;
        }
    }
    return CmnStatus::FAILURE;
}

