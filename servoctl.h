/* Servo control functions.
 * All "servo_ids" are the arduino pin number!
 */

#ifndef DSESW_SERVOCTL_H
#define DSESW_SERVOCTL_H

#include "cmn.h"

void servoctl_init();

// Config:
CmnStatus servoctl_set_softstops(U8 servo_id, U8 stop_low, U8 stop_high);
CmnStatus servoctl_set_open_pos(U8 servo_id, U8 position);
CmnStatus servoctl_set_close_pos(U8 servo_id, U8 position);
CmnStatus servoctl_save_config();
void servoctl_dump_config();

// Servo motion:
CmnStatus servoctl_move_servo(U8 servo_id, U8 position);
CmnStatus servoctl_open_servo(U8 servo_id);
CmnStatus servoctl_close_servo(U8 servo_id);
CmnStatus servoctl_stop_servo(U8 servo_id);
CmnStatus servoctl_stop_all_servos();
CmnStatus servoctl_abort();

#endif
