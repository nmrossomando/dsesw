#ifndef DSESW_BEHAVIORS_H
#define DSESW_BEHAVIORS_H

#include "cmn.h"
#include "actions.h"
#include "eep.h"

// Behaviors are primarily called by I2C commands, but BEHAVIORS_EXECUTE will let you call one from a laptop via UART.
// Other UART commands allow you to create new behaviors in EEP without new firmware.

// Behavior tables are always 32 bytes - "empty" rows use Action NONE, which tells the executor that it's done.
struct BehaviorTable {
    Action actions[16]; // EEP_MAX_RECORD_SIZE / 2 (action struct is 2 bytes)
};

void behaviors_init();
void behaviors_main();

CmnStatus behaviors_execute_behavior(U8 behavior_id);
CmnStatus behaviors_continue_behavior();
CmnStatus behaviors_run_next_step();
void behaviors_pause_behavior(U32 resume_time);
CmnStatus behaviors_stop_behavior(CmnStatus status);
bool behaviors_is_behavior_executing();

// Behavior creation group.
// Intended conops: BEHAVIORS_CREATE_BEHAVIOR command comes in to create a new behavior at the given id.
// Then, you use a BEHAVIORS_CREATE_ADD_ACTION command that adds to the "open" behavior.
// Then, you use BEHAVIORS_CREATE_COMMIT_BEHAVIOR to save it to EEPROM.
// A new BEHAVIORS_CREATE_BEHAVIOR will reset the progress. A BEHAVIORS_CREATE_CANCEL command will abort the process.
// This conops keeps individual command sizes small, saving RAM.
CmnStatus behaviors_create_new_behavior(U8 behavior_id);
CmnStatus behaviors_new_behavior_add_action(ActionTypes action, U8 arg);
CmnStatus behaviors_commit_new_behavior();
CmnStatus behaviors_create_cancel();

#endif