/* Process one-byte "commands" that come in over I2C from the stealth.
 * Will buffer commands, and space them out by parameterized time.
 * Will also ignore commands for a parameterized time after startup.
 */
#ifndef DSESW_BUSCMD_H
#define DSESW_BUSCMD_H

#include "cmn.h"

void buscmd_init(U8 i2c_addr, U8 startup_ignore_dur, U8 cmd_spacing_dur);
void buscmd_main();

#endif
