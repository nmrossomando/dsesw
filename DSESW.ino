#include "cmn.h"
#include "eep.h"
#include "UART.h"
#include "tlm.h"
#include "buscmd.h"
#include "behaviors.h"
#include "servoctl.h"
#include "cmd.h"

struct SystemConfig {
    U8 i2c_addr; // I2C address to listen for commands on.
    U8 secs_to_boot; // Seconds to wait for the stealth to boot before accepting I2C commands. I saw an errant command at stealth boot so I'm implementing this as a workaround.
    U8 behavior_interval; // Minimum seconds between behaviors, in case we run in to issues going back-to-back with stuff.
};

SystemConfig config;

void setup() {
    // Do early init for tlm and uart:
    tlm_init();
    uart_init();

    TLM_LOG_MESSAGE_NO_ARGS(LogLevels::ACTIVITY, LogMessages::SYS_LOG_INIT, "DSESW Initializing.");
    // Get the system config in a known good default state:
    config.i2c_addr = 42;
    config.secs_to_boot = 10;
    config.behavior_interval = 1;

    // Then load config from EEPROM to recover saved values:
    EepRecord raw_config;
    eep_get_record(EEP_RECORDS::SYSTEM_CONFIGURATION, raw_config);
    if((raw_config.data[0] == 255) && (raw_config.data[1] == 255) && (raw_config.data[2]) == 255) {
        // If the EEP is still "raw", write the sane defaults out:
        raw_config.data[0] = config.i2c_addr;
        raw_config.data[1] = config.secs_to_boot;
        raw_config.data[2] = config.behavior_interval;
        eep_write_record(EEP_RECORDS::SYSTEM_CONFIGURATION, raw_config);
    }
    else {
        // Otherwise, restore the saved values:
        config.i2c_addr = raw_config.data[0];
        config.secs_to_boot = raw_config.data[1];
        config.behavior_interval = raw_config.data[2];
        TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::SYS_LOG_LOADED_CONFIG, "Loaded Config: i2c Address %u, secs to boot %u, behavior interval %u", config.i2c_addr, config.secs_to_boot, config.behavior_interval);
    }

    // Now start doing the rest of system init:
    servoctl_init();
    behaviors_init();
    cmd_init();
    buscmd_init(config.i2c_addr, config.secs_to_boot, config.behavior_interval);
}

void loop() {
    behaviors_main();
    cmd_main();
    buscmd_main();
    tlm_xmit();
    delay(10);
}