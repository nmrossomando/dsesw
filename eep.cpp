#include "eep.h"

#include <EEPROM.h>

#include "tlm.h" 

CmnStatus eep_get_record(EEP_RECORDS record_id, EepRecord& record) {
    U16 address = static_cast<U8>(record_id) * EEP_RECORD_MAX_SIZE;
    if(address > (EEP_SIZE - EEP_RECORD_MAX_SIZE)) {
        return CmnStatus::FAILURE;
    }

    EEPROM.get(address, record);
    return CmnStatus::SUCCESS;
}

CmnStatus eep_write_record(EEP_RECORDS record_id, const EepRecord& record) {
    U16 address = static_cast<U8>(record_id) * EEP_RECORD_MAX_SIZE;
    if(address > (EEP_SIZE - EEP_RECORD_MAX_SIZE)) {
        return CmnStatus::FAILURE;
    }

    EEPROM.put(address, record);
    return CmnStatus::SUCCESS;
}

CmnStatus eep_peek(U16 addr) {
    U8 result = EEPROM.read(addr);
    TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::EEP_LOG_PEEK_ADDRESS, "EEPROM address %u contains value %u.", addr, result);
    return CmnStatus::SUCCESS;
}

CmnStatus eep_poke(U16 addr, U8 val) {
    TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::EEP_LOG_POKE_ADDRESS, "EEPROM address %u written to value %u.", addr, val);
    EEPROM.write(addr, val);
    return CmnStatus::SUCCESS;
}

CmnStatus eep_dump_record(EEP_RECORDS record_id) {
    EepRecord record;
    CmnStatus stat = eep_get_record(record_id, record);
    if(stat == CmnStatus::FAILURE) {
        return CmnStatus::FAILURE;
    }

    for(U8 i=0; i<32; i++) {
        TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::EEP_LOG_RECORD_DUMP, "Record %s index %u has value %u.", static_cast<U8>(record_id), i, record.data[i]);
    }

    return CmnStatus::SUCCESS;
}

CmnStatus eep_wipe_record(EEP_RECORDS record_id) {
    EepRecord default_vals;
    for(auto& val : default_vals.data) {
        val = 255;
    }

    CmnStatus stat = eep_write_record(record_id, default_vals);
    if(stat == CmnStatus::FAILURE) {
        TLM_LOG_MESSAGE(LogLevels::WARNING_HI, LogMessages::EEP_LOG_RECORD_WIPE_FAILED, "Wipe of EEPROM record %s failed! Data may be in unknown state!", static_cast<U8>(record_id));
        return CmnStatus::FAILURE;
    }

    TLM_LOG_MESSAGE(LogLevels::ACTIVITY, LogMessages::EEP_LOG_RECORD_WIPED, "EEPROM record %s wiped back to default state!", static_cast<U8>(record_id));
    return CmnStatus::SUCCESS;
}
